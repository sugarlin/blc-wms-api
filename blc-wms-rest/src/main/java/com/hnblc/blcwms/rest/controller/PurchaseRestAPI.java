package com.hnblc.blcwms.rest.controller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.hnblc.blcwms.common.constant.StringPool;
import com.hnblc.blcwms.common.exceptions.ErrorDescription;
import com.hnblc.blcwms.common.exceptions.GeneralException;
import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.common.interaction.RestResponse;
import com.hnblc.blcwms.common.utils.encrypt.BASE64;
import com.hnblc.blcwms.persistent.interfaces.business.entity.ExpLog;
import com.hnblc.blcwms.persistent.interfaces.business.service.IExpLogService;
import com.hnblc.blcwms.persistent.interfaces.client.dto.CurrentClient;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.persistent.interfaces.message.entity.MessageLog;
import com.hnblc.blcwms.rest.dto.restInteraction.EncryptRequest;
import com.hnblc.blcwms.rest.service.IMessageService;
import com.hnblc.blcwms.serviceapi.api.dto.WareHouseOperate;
import com.hnblc.blcwms.serviceapi.api.dto.purchase.PurchaseBody;
import com.hnblc.blcwms.serviceapi.api.dto.purchase.PurchaseHead;
import com.hnblc.blcwms.serviceapi.api.enums.code.ExpLogTypeEnum;
import com.hnblc.blcwms.serviceapi.api.group.owner.purchase.PurchaseGroup;
import com.hnblc.blcwms.serviceapi.api.service.IPurchaseNoteAPIService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

@Setter
@Getter
@RestController
@RequestMapping("blc/wms/api/purchase")
@Api(value="采购进货处理REST API",description = "货主采购进货通知单相关操作",tags={"入库单操作接口"})
@Validated
public class PurchaseRestAPI extends BaseRestAPI{



    @Autowired
    IExpLogService expLogService;
    @Autowired
    IMessageService messageService;
    @Autowired
    IPurchaseNoteAPIService purchaseNoteService;

    Logger logger = LoggerFactory.getLogger(PurchaseRestAPI.class);

    @ApiOperation(value="创建采购入库单", notes="客户ERP采购通知单生成入库单",position = 1)
    @RequestMapping(value = "/create" ,method = RequestMethod.POST)
    @ResponseBody
    public RestResponse createPurchase(@Validated(PurchaseGroup.class)@RequestBody EncryptRequest message, HttpServletRequest request) throws IOException {
        //测试打印原始报文
        String jsonString = IOUtils.toString(request.getInputStream(), Charset.defaultCharset());
        logger.debug("***********************************create Purchase test start***************************\n"+jsonString+"\n*********************end*************************");

        //解析业务数据 从BASE64字符串解析为JSON字符串，再解析为业务对象
        WareHouseOperate<PurchaseHead> businessData;
        try {
            businessData = objectMapper.readValue(new String(BASE64.decryptBASE64(message.getBusinessData()), StringPool.CHARSET_UTF8),new TypeReference<WareHouseOperate<PurchaseHead>>() { });
        }catch (Exception e){
            e.printStackTrace();
            return new RestResponse<>(new GeneralException(ErrorDescription.ERROR_REQUEST_BAD_PARAMETER_FORMAT_0,e));
        }

        //简单验证
        this.validate(businessData, PurchaseGroup.class);

        //校验操作权限
        ClientAuth clientAuth = this.buildAuth(message,businessData);
        //货主权限，客户专用位
        clientAuth.setCustomNo("N");
        CurrentClient currentClient = this.clientInfoService.loadCurrentClient(clientAuth);
        if (null == currentClient || currentClient.getClientAuthInfo().size() < 1) {
            return new RestResponse<>(new GeneralException(ErrorDescription.ERROR_REQUEST_CLIENT_UNAUTHORIZED_0));
        }

        //验签
        try {
            if (!this.signatureValidate(message, currentClient)) {
                return new RestResponse<>(new GeneralException(ErrorDescription.ERROR_REQUEST_SIGNATURE_VALIDATE_FAIL_0));
            }
        } catch (Exception e) {
            logger.error("接口处理验签异常", e);
            throw new GeneralException(e);
        }
        MessageLog messageLog = messageService.process(currentClient, request);
        //TODO 设置基础资料更新日志

        //创建采购单
        Response<List<PurchaseBody>> result= purchaseNoteService.createPurchaseNote(businessData.getOperateDetails(),currentClient.getClientAuthInfo().get(0));
        //保存创建日志，如果没有则无法反馈发货状态
        ExpLog expLog = new ExpLog(ExpLogTypeEnum.PURCHASE.code(),message.getClientId(),messageLog.getSeqNo(),businessData.getOperateDetails().getPurchaseNo(),result);
        if(!expLogService.save(expLog)){
            logger.error("Error occurred when save [RATION NOTE] creation log to database");
        }
        messageService.processed(messageLog);

        return new RestResponse(result);
    }



    @ApiOperation(value="创建退厂出库单", notes="客户ERP向WMS发出退厂出库通知单",position = 2)
    @RequestMapping(value = "/retreat" ,method = RequestMethod.POST)
    @ResponseBody
    public RestResponse createRetread(@Validated(PurchaseGroup.class)@RequestBody EncryptRequest message, HttpServletRequest request) throws IOException {

        return null;
    }


}
