package com.hnblc.blcwms.rest.logistics.enums;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseEnums;
import lombok.Getter;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Getter
public enum LogisticCompanyEnum implements BaseEnums {



    YTO("1","圆通","YTO"),
    STO("2","申通","STO"),
    SF("3","顺丰","SF"),
    PDD_YTO("4","拼多多圆通","PDD_YTO");


    private final String code;
    private final String textCn;
    private final String textEn;

    LogisticCompanyEnum(String code, String textCn, String testEn){
        this.code = code;
        this.textCn = textCn;
        this.textEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }


    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.textEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (LogisticCompanyEnum enums : EnumSet.allOf(LogisticCompanyEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
