package com.hnblc.blcwms.rest.logistics.vo;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming
public class PDDPrintData {
    private String waybillCode;
    private String wpCode;
    private RoutingInfo routingInfo;
    private Person sender;
    private Person recipient;
    private String signature;
    private String templateUrl;






    @Data
    @JsonNaming
    public static class RoutingInfo{
        private String requestId;
        private String objectId;
        private String originBranchCode;
        private String originBranchName;
        private String bigShotCode;
        private String bigShotName;
        private String endBranchCode;
        private String endBranchName;
        private String threeSegmentCode;
        private String threeSegmentCodeSource;
    }

    @Data
    @JsonNaming
    public static class Person{
        private Address address;
        private String mobile;
        private String phone;
        private String name;

    }
    @Data
    @JsonNaming
    public static class Address{
        private String province;
        private String city;
        private String district;
        private String detail;
        private String town;
    }


}
