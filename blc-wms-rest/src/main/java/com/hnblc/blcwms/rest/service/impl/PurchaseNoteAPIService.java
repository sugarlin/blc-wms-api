package com.hnblc.blcwms.rest.service.impl;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsPurchase;
import com.hnblc.blcwms.persistent.interfaces.business.service.IExpLogService;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsPurchaseService;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.serviceapi.api.dto.purchase.PurchaseBody;
import com.hnblc.blcwms.serviceapi.api.dto.purchase.PurchaseHead;
import com.hnblc.blcwms.serviceapi.api.enums.result.businiss.PurchaseNoteResultEnum;
import com.hnblc.blcwms.serviceapi.api.service.IPurchaseNoteAPIService;
import com.hnblc.blcwms.serviceapi.definition.IBaseDefineService;
import com.hnblc.blcwms.serviceapi.idata.enums.code.ImportTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PurchaseNoteAPIService implements IPurchaseNoteAPIService {

    Logger logger = LoggerFactory.getLogger(PurchaseNoteAPIService.class);
    @Autowired
    IWmsPurchaseService wmsPurchaseService;
    //TODO 调用业务数据库验证基础资料
    @Autowired
    IBaseDefineService baseDefineService;


    @Override
    public Response<List<PurchaseBody>> createPurchaseNote(PurchaseHead purchaseHead, ClientAuth clientAuth) {
        List<WmsPurchase> purchases = new ArrayList<>();
        //验证进货详情是否为空集合
        if (purchaseHead.getPurchaseDetails()==null || purchaseHead.getPurchaseDetails().size()<1){
            return new Response<>(PurchaseNoteResultEnum.ERROR_EMPTY_ARTICLE_ARRAY);
        }
        //判断进货类型是否存在
        boolean typeExist = false;
        for (ImportTypeEnum qualityEnum: ImportTypeEnum.values()){
            if (qualityEnum.code().equalsIgnoreCase(purchaseHead.getPurchaseType())){
                typeExist = true;
            }
        }
        if (!typeExist){
            return new Response<>(PurchaseNoteResultEnum.ERROR_WRONG_PURCHASE_TYPE);
        }

        List<PurchaseBody> errorBodies = new ArrayList<>();
        for (PurchaseBody purchaseBody:purchaseHead.getPurchaseDetails()){
            if (baseDefineService.getArticleCache(clientAuth.getOwnerNo(),purchaseBody.getOwnerArticleNo())==null){
                purchaseBody.setErrorReason(PurchaseNoteResultEnum.ERROR_UNKNOWN_SKU.getMessage());
                errorBodies.add(purchaseBody);
            }
            purchases.add(this.buildWmsPurchase(purchaseBody,purchaseHead,clientAuth));
        }

        //错误商品不为空，则反馈错误，不入库
        if (errorBodies.size()>0){
            return new Response<>(PurchaseNoteResultEnum.ERROR_UNKNOWN_SKU,errorBodies);
        }
        //未读取到合适的入库商品明细，不入库
        if (purchases.size()<1){
            return new Response<>(PurchaseNoteResultEnum.ERROR_READ_FAIL);
        }

        if (!wmsPurchaseService.saveBatch(purchases)){
            logger.error("Error occurred when save article categories sheetid:"+purchaseHead.getPurchaseNo());
            return new Response<>(PurchaseNoteResultEnum.ERROR_SAVE_FAIL);
        }
        return new Response<>(PurchaseNoteResultEnum.SUCCESS);
    }


    /**
     *
     * @param body
     * @param head
     * @param clientAuth
     * @return
     */
    private WmsPurchase buildWmsPurchase(PurchaseBody body,PurchaseHead head,ClientAuth clientAuth){
        WmsPurchase wmsPurchase = new WmsPurchase();
        wmsPurchase.setCustomid(clientAuth.getOwnerNo());
        wmsPurchase.setShopNo(clientAuth.getOwnerNo());
        wmsPurchase.setSheetid(head.getPurchaseNo());
        wmsPurchase.setPalletzone(clientAuth.getWarehouseNo());
        wmsPurchase.setChecker(head.getChecker());
        wmsPurchase.setPurdate(head.getCheckDate());
        wmsPurchase.setSdate(LocalDateTime.now());
        wmsPurchase.setNotes(head.getRemark());
        wmsPurchase.setGoodsid(body.getOwnerArticleNo());
        wmsPurchase.setSerialid(body.getItemIndex());
        wmsPurchase.setPkcount(body.getPackageQty());
        wmsPurchase.setQty(body.getPlanQty());
        return wmsPurchase;
    }
}
