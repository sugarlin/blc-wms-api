package com.hnblc.blcwms.rest.logistics.enums;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum WaybillResultEnum implements BaseResultEnums {



    SUCCESS(true,"10","获取成功","SUCCESS"),
    ERROR_EXP_NOT_FOUND(false,"11","未查询到指定出货单信息","ERROR_EXP_NOT_FOUND"),
    ERROR_WRONG_LOGISTIC_CODE(false,"12","不存在此物流商代码：{0}","ERROR_WRONG_LOGISTIC_CODE"),
    ERROR_LOGISTIC_CODE(false,"12","此物流商服务暂未开通：{0}","ERROR_LOGISTIC_CODE"),
    PDD_ERROR_GET_BRANCH_FAIL(false,"21","获取拼多多物流商[{0}]网点信息失败：{1}","PDD_ERROR_GET_BRANCH_FAIL"),
    PDD_ERROR_EXP_WRONG_SOURCE(false,"22","出货单[{0}]非拼多多的订单，无法获取拼多多专用单号：{1}","PDD_ERROR_EXP_WRONG_SOURCE"),
    PDD_ERROR_GET_LOGISTIC_FAIL(false,"23","获取拼多多物流商[{0}]代码失败：{1}","PDD_ERROR_GET_LOGISTIC_FAIL"),
    PDD_ERROR_GET_TEMPLATE_FAIL(false,"24","获取拼多多物流商[{0}]标准模板失败：{1}","PDD_ERROR_GET_TEMPLATE_FAIL"),
    PDD_ERROR_GET_WAYBILL_FAIL(false,"25","获取拼多多电子面单失败：参数[{0}][{1}]，错误详情：{2}","PDD_ERROR_GET_WAYBILL_FAIL"),
    PDD_ERROR_CANCEL_WAYBILL_FAIL(false,"26","取消拼多多物流商[{0}]运单号[{1}]失败：{2}","PDD_ERROR_CANCEL_WAYBILL_FAIL"),
    //    ERROR_REPEAT_EXP_NO(false,"15","查出多条出货单记录，无法获取运单号","ERROR_REPEAT_EXP_NO"), //开放批量获取后，此条作废
    ERROR_UNKNOWN(false,"99","未知错误：{0}","ERROR_UNKNOWN");


    private String code;
    private String category="71";  //获取物流信息错误
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    WaybillResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (WaybillResultEnum enums : EnumSet.allOf(WaybillResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
