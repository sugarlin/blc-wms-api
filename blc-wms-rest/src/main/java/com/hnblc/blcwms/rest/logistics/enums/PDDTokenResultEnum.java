package com.hnblc.blcwms.rest.logistics.enums;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum PDDTokenResultEnum implements BaseResultEnums {



    SUCCESS(true,"10","获取成功","SUCCESS"),
//    PDD_ERROR_GET_LOGISTIC_FAIL(false,"11","获取物流商{0}拼多多代码失败：{1}","PDD_ERROR_GET_LOGISTIC_FAIL"),
    ERROR_UNKNOWN(false,"99","未知错误：{0}","ERROR_UNKNOWN");


    private String code;
    private String category="61";  //订单出货类型错误
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    PDDTokenResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (PDDTokenResultEnum enums : EnumSet.allOf(PDDTokenResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
