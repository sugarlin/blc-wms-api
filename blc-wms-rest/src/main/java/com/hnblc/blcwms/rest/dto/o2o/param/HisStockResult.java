package com.hnblc.blcwms.rest.dto.o2o.param;

import com.hnblc.blcwms.persistent.business.stock.entity.HisStock;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class HisStockResult {

    private String storeCode;
    private String endDate;
    private List<HisStock> stockDetail;
}
