package com.hnblc.blcwms.rest.logistics.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressInfoHead;
import com.hnblc.blcwms.serviceapi.api.dto.logistics.WaybillInfo;

import java.util.List;
/**
 * 通过获取运单号段方式获取运单号的接口类 <br>
 * get waybill number segment then upload order information
 * @author  linsong <br>
 * @date 2020.01.02 18:18 <br>
 */
public interface ILogisticServiceTypeB {
    /**
     * 根据订单信息获取运单信息
     * @param expressInfoHead 需要获取运单号的单据信息集合
     * @return Response<List<WaybillInfo>> 运单消息返回结果
     */
    Response<WaybillInfo> getWaybillInfo(ExpressInfoHead expressInfoHead);

    /**
     * 根据订单信息批量获取运单信息
     * @param expressInfoHeads 需要获取运单号的单据信息集合
     * @return Response<List<WaybillInfo>> 运单消息返回结果
     */
    Response<List<WaybillInfo>> getWaybillInfos(List<ExpressInfoHead> expressInfoHeads);
    /**
     * 获取运单号段
     * @param count 获取运单号段的数量
     * @return Response<List<String>> 运单号段
     */
    Response<List<String>> getWaybillNos(Integer count);
}
