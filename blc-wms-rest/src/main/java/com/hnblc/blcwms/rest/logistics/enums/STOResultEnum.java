package com.hnblc.blcwms.rest.logistics.enums;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum STOResultEnum implements BaseResultEnums {



    SUCCESS(true,"10","获取成功","SUCCESS"),
    NO_NOT_ENOUGH(true,"11","部分成功，本次获取单号不足","WAYBILL NOS NOT ENOUGH"),
    EMPTY_NO_DATA(false,"12","获取申通热敏号段为空","EMPTY DATA WHEN GET WAYBILL NO"),
    ERROR_RES_GET_NO(false,"13","获取运单号段时出错:{0}","ERROR WHEN GET WAYBILL NO:{0}"),
    ERROR_RES_UPLOAD_INFO(false,"13","上传运单信息时出错:{0}","ERROR WHEN UPLOAD WAYBILL INFO:{0}"),
    NOT_ALL_SUC(true,"14","部分成功，部分失败原因：{0}","PART SUCCESS,PART FAIL:{0}"),
    ERROR_UNKNOWN(false,"99","未知错误：{0}","ERROR_UNKNOWN");


    private String code;
    private String category="62";  //订单出货类型错误
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    STOResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (STOResultEnum enums : EnumSet.allOf(STOResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
