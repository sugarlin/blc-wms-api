package com.hnblc.blcwms.rest.logistics.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressInfoHead;
import com.hnblc.blcwms.serviceapi.api.dto.logistics.WaybillInfo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description <br>
 * @ClassName YTOService <br>
 * @Author linsong <br>
 * @LocalDateTime 2019.12.11 11:27 <br>
 */
@Service
public class YTOService implements ILogisticServiceTypeA {


    @Override
    public Response<List<WaybillInfo>> getWaybillInfo(List<ExpressInfoHead> expressInfoHeads) {


        /*Map<String,RequestOrder> requestMap = new HashMap<String,RequestOrder>();
        for(OrderDetail orderDetail : orderList){
            String key = orderDetail.getExpNo();
            RequestOrder.Item item = new RequestOrder.Item(orderDetail.getGoodName(),orderDetail.getGootQty().toString(),orderDetail.getGoodValue().toString());
            Double goodValue = orderDetail.getGootQty()*orderDetail.getGoodValue();

            if(requestMap.containsKey(key)){
                if(requestMap.get(orderDetail.getExpNo()).getItem()!=null){
                    RequestOrder tmpOrder = requestMap.get(orderDetail.getExpNo());
                    tmpOrder.getItem().add(item);
                    tmpOrder.setGoodsValue(tmpOrder.getGoodsValue()+goodValue);
                }else{
                    List<RequestOrder.Item> items = new ArrayList<RequestOrder.Item>();
                    items.add(item);
                    requestMap.get(orderDetail.getExpNo()).setItem(items);
                }
            }else{
                RequestOrder request = new RequestOrder();
                RequestOrder.Sender sender = new RequestOrder.Sender(orderDetail.getSendName(),orderDetail.getSendPostCode(),orderDetail.getSendTelephone(),orderDetail.getSendTelephone(),orderDetail.getSendProvince(),orderDetail.getSendCity()+","+orderDetail.getSendZone(),orderDetail.getSendAddress());
                RequestOrder.Receiver receiver = new RequestOrder.Receiver(orderDetail.getReceiver(),orderDetail.getReceiverPostCode(),orderDetail.getReceiverTel(),orderDetail.getReceiverTel(),orderDetail.getReceiverProvice(),orderDetail.getReceiverCity()+","+orderDetail.getReceiverZone(),orderDetail.getReceiverAddress());
                List<RequestOrder.Item> items = new ArrayList<RequestOrder.Item>();
                items.add(item);

                request.setSendEndTime(new LocalDateTime());
                request.setSendStartTime(DateUtil.getFutureDate(1));
                request.setGoodsValue(goodValue);
                request.setTxLogisticID( SysCons.clientId+orderDetail.getOrderNo());
                request.setSender(sender);
                request.setReceiver(receiver);
                request.setItem(items);

                requestMap.put(key, request);
            }

        }

        JAXBContext requsetContext = null;
        JAXBContext responseContext = null;
        Marshaller marshaller = null;
        Unmarshaller unmarshaller = null;
        try {
            requsetContext = JAXBContext.newInstance(RequestOrder.class);

            responseContext = JAXBContext.newInstance(Response.class);

            marshaller = requsetContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

            unmarshaller = responseContext.createUnmarshaller();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Iterator<String> it = requestMap.keySet().iterator();
        StringBuilder errorExpNo = new StringBuilder();
        StringBuilder errorDetail = new StringBuilder();
        List<Map<String,String>> responseList = new ArrayList<Map<String,String>>();
        while(it.hasNext()){
            String key2 = it.next();
            Response res = null;
            StringWriter xmlWriter = new StringWriter();
            try {

                marshaller.marshal(requestMap.get(key2), xmlWriter);
                Map<String,String> param = new HashMap<String,String>();

                param.put("logistics_interface",xmlWriter.toString());

                param.put("data_digest", MD5Util.getMD5EncodeBase64(xmlWriter.toString()+SysCons.partnerId));
                param.put("clientId",  SysCons.clientId);
                res = (Response)unmarshaller.unmarshal(new StringReader(HttpClientUtil.sendRequest(SysCons.ytoUrl, param, "2")));

            } catch (JAXBException e) {
                errorExpNo.append(key2+";");
                errorDetail.append("生成或解析报文异常:"+e.getMessage()+";");
                e.printStackTrace();
                continue;
            }catch(UnsupportedEncodingException e){
                errorExpNo.append(key2+";");
                errorDetail.append("URLENCODE异常:"+e.getMessage()+";");
                e.printStackTrace();
                continue;
            }catch(ClientProtocolException e){
                errorExpNo.append(key2);
                errorDetail.append("远程请求圆通接口异常:"+e.getMessage()+";");
                e.printStackTrace();
                continue;
            }catch(IOException e){
                errorExpNo.append(key2);
                errorDetail.append("远程请求IO异常:"+e.getMessage()+";");
                e.printStackTrace();
                continue;
            }
            if("200".equalsIgnoreCase(res.getCode())){
                Map<String,String> updateData = new HashMap<String,String>();
                updateData.put("expNo", key2);
                updateData.put("waybillNo", res.getMailNo());
                updateData.put("deleveryAddress", res.getDistributeInfo().getShortAddress());
                responseList.add(updateData);
            }else{
                errorExpNo.append(key2+";");
                errorDetail.append("请求失败:"+key2+":"+res.getReason()+";<br/>");
            }
        }*/
        return null;
    }


    @Override
    public Response<List<WaybillInfo>> getWaybillInfo(List<ExpressInfoHead> expressInfoHeads, boolean isEncrypt) {
        return null;
    }

    @Override
    public Response<List<WaybillInfo>> getWaybillInfo(List<ExpressInfoHead> expressInfoHeads, boolean isEncrypt, String logisicCodeCn) {
        return null;
    }

    @Override
    public Response<Boolean> cancelWaybillNo(String logisticCode, String wayBillNo) {
        return null;
    }



  /*  @XmlRootElement(name="RequestOrder")
    @XmlAccessorType(XmlAccessType.FIELD)
    @Data
    public static class RequestOrder {

        private String clientID = "";
        private String logisticProviderID = "YTO";
        private String customerId =  "";
        private String txLogisticID;
        private String tradeNo;
        private String totalServiceFee = "0.0";
        private String codSplitFee ="0.0";
        private String orderType = "0";
        private String serviceType = "0";
        private String flag = "1";
        private String agencyFund = "0.0";

        private RequestOrder.Sender sender;
        private RequestOrder.Receiver receiver;

        @XmlJavaTypeAdapter(DateAdapter.class)
        private LocalDateTime sendStartTime;
        @XmlJavaTypeAdapter(DateAdapter.class)
        private LocalDateTime sendEndTime;

        private Double goodsValue;

        @XmlElementWrapper(name="Items")
        @XmlElement
        private List<Item> Item;

        private String insuranceValue = "0.0";

        private String special = "0";

        private String remark = "河南保税物流中心圆通专用单号申请";

        @XmlRootElement(name="sender")
        @XmlAccessorType(XmlAccessType.FIELD)
        @Data
        public static class Sender{
            private String name;
            private String postCode;
            private String phone;
            private String mobile;
            private String prov;
            private String city;
            private String address;

            public Sender() {
                super();
            }
            public Sender(String name, String postCode, String phone,
                          String mobile, String prov, String city, String address) {
                super();
                this.name = name;
                this.postCode = postCode;
                this.phone = phone;
                this.mobile = mobile;
                this.prov = prov;
                this.city = city;
                this.address = address;
            }
        }

        @XmlRootElement(name="receiver")
        @XmlAccessorType(XmlAccessType.FIELD)
        @Data
        public static class Receiver{
            private String name;
            private String postCode;
            private String phone;
            private String mobile;
            private String prov;
            private String city;
            private String address;


            public Receiver() {
                super();
                // TODO Auto-generated constructor stub
            }
            public Receiver(String name, String postCode, String phone,
                            String mobile, String prov, String city, String address) {
                super();
                this.name = name;
                this.postCode = postCode;
                this.phone = phone;
                this.mobile = mobile;
                this.prov = prov;
                this.city = city;
                this.address = address;
            }

        }

        @XmlRootElement(name="item")
        @XmlAccessorType(XmlAccessType.FIELD)
        @Data
        public static class Item{
            private String itemName;
            private String number;
            private String itemValue;


            public Item() {
                super();
                // TODO Auto-generated constructor stub
            }
            public Item(String itemName, String number, String itemValue) {
                super();
                this.itemName = itemName;
                this.number = number;
                this.itemValue = itemValue;
            }

        }
    }*/
}
