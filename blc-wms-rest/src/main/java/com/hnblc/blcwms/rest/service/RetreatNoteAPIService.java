package com.hnblc.blcwms.rest.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsRetService;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.serviceapi.api.dto.ret.note.RetreatNoteBody;
import com.hnblc.blcwms.serviceapi.api.dto.ret.note.RetreatNoteHead;
import com.hnblc.blcwms.serviceapi.api.service.IRetreatNoteAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>Description: 退厂通知单实现类</p> <br/>
 *
 * @author linsong <br>
 * @version 1.0 2020.01.20 17:49 <br>
 */
@Service
public class RetreatNoteAPIService implements IRetreatNoteAPIService {


    @Autowired
    IWmsRetService wmsRetService;

    @Override
    public Response<List<RetreatNoteBody>> createRetNote(RetreatNoteHead retreatNoteHead, ClientAuth clientAuth) {

        wmsRetService.saveBatch();
        return null;
    }
}
