package com.hnblc.blcwms.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan("com.hnblc.blcwms")
@EnableCaching
@ServletComponentScan
@EnableScheduling
public class BlcWmsRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlcWmsRestApplication.class, args);
    }


}
