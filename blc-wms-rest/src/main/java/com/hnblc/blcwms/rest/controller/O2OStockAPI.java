package com.hnblc.blcwms.rest.controller;

import com.hnblc.blcwms.common.exceptions.ErrorDescription;
import com.hnblc.blcwms.common.exceptions.GeneralException;
import com.hnblc.blcwms.persistent.business.stock.dto.StockHisQuery;
import com.hnblc.blcwms.persistent.interfaces.client.dto.CurrentClient;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.persistent.interfaces.client.service.IClientInfoService;
import com.hnblc.blcwms.rest.dto.o2o.O2ORequest;
import com.hnblc.blcwms.rest.dto.o2o.O2OResponse;
import com.hnblc.blcwms.rest.dto.o2o.param.HisStockResult;
import com.hnblc.blcwms.serviceapi.api.dto.WareHouseOperate;
import com.hnblc.blcwms.serviceapi.api.service.IStockAPIService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping(value = "blc/wms/o2o/api")
@Api(value = "河南保税跨境O2O系统--仓储接口服务",description = "提供河南保税跨境O2O线下自提仓储系统内容查询",tags={"O2O库存接口服务"})
public class O2OStockAPI extends BaseRestAPI {


    @Autowired
    IStockAPIService stockService;
    @Autowired
    IClientInfoService clientInfoService;

    @ApiOperation(value="查询历史库存", notes="历史某日当日剩余库存")
    @PostMapping(value="stock/history")
    @ResponseBody
    public O2OResponse<HisStockResult> getHistoryStock(@RequestBody O2ORequest<WareHouseOperate<StockHisQuery>> request){
        O2OResponse<HisStockResult> res = new O2OResponse(true);


        ClientAuth clientAuth = this.buildAuth(request);
        CurrentClient currentClient = this.clientInfoService.loadCurrentClient(clientAuth);
        if (null == currentClient || currentClient.getClientAuthInfo().size()<1) {
            res.initError(new GeneralException(ErrorDescription.ERROR_REQUEST_CLIENT_UNAUTHORIZED_0));
            return res;
        }
        //签名验证
        StockHisQuery stockHisQuery = request.getBusinessData().getOperateDetails();
        try {
            if (!this.signatureValidate(request,currentClient)){
                res.initError(new GeneralException(ErrorDescription.ERROR_REQUEST_SIGNATURE_VALIDATE_FAIL_0));
                return res;
            }
        }catch (Exception e) {
            res.initError(new GeneralException(ErrorDescription.ERROR_REQUEST_NOT_IN_CHARSET));
            return res;
        }


        HisStockResult hsr = new HisStockResult();
        hsr.setEndDate(stockHisQuery.getEndDate());
        hsr.setStoreCode(stockHisQuery.getStoreCode());
        hsr.setStockDetail(stockService.listHistoryStock(stockHisQuery));
        res.setBusiness_data(hsr);


//        List<HisStock> testList = new ArrayList<>();
//        HisStock hs1 = new HisStock();
//        hs1.setGoodsName("超强固体胶");
//        hs1.setItemNo("000001");
//        hs1.setGoodsNo("G000001");
//        hs1.setRemainQty("7124");
//        hs1.setUnit("个");
//        hs1.setStoreInfo("A-1-11-1,A-2-12-2");
//        HisStock hs2= new HisStock();
//        hs2.setGoodsName("自粘便条纸");
//        hs2.setItemNo("000002");
//        hs2.setGoodsNo("G000002");
//        hs2.setRemainQty("5214");
//        hs2.setUnit("袋");
//        hs2.setStoreInfo("A-1-11-1");
//        HisStock hs3= new HisStock();
//        hs3.setGoodsName("新增门店系统未进货商品");
//        hs3.setItemNo("000003");
//        hs3.setGoodsNo("G000003");
//        hs3.setRemainQty("5131");
//        hs3.setUnit("袋");
//        hs3.setStoreInfo("A-1-11-1");
//        testList.add(hs3);
//        testList.add(hs1);
//        testList.add(hs2);
//        hsr.setStockDetail(testList);
//        res.setBusiness_data(hsr);
        res.setSessionId(request.getSessionId());
        return res;
    }

}
