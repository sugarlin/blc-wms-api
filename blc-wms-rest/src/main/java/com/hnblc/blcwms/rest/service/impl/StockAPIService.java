package com.hnblc.blcwms.rest.service.impl;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.business.stock.dto.StockHisQuery;
import com.hnblc.blcwms.persistent.business.stock.entity.HisStock;
import com.hnblc.blcwms.persistent.interfaces.business.dto.StockParam;
import com.hnblc.blcwms.persistent.interfaces.business.entity.SalableStock;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.StockAPIMapper;
import com.hnblc.blcwms.serviceapi.api.enums.result.businiss.StockQueryResultEnum;
import com.hnblc.blcwms.serviceapi.api.service.IStockAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockAPIService implements IStockAPIService {


    @Autowired
    StockAPIMapper stockAPIMapper;
    @Override
    public List<HisStock> listHistoryStock(StockHisQuery stockHisQuery) {
//        return stockAPIMapper.queryHisStock(stockHisQuery);
        return null;
    }

    @Override
    public Response<List<SalableStock>> getSalableStocks(StockParam stockParam) {
        List<SalableStock> salableStocks = stockAPIMapper.getSalableStocks(stockParam);
        if (salableStocks.size()>0)
            return new Response<>(StockQueryResultEnum.SUCCESS,salableStocks);
        else
            return new Response<>(StockQueryResultEnum.NOT_FOUND,salableStocks);
    }
}
