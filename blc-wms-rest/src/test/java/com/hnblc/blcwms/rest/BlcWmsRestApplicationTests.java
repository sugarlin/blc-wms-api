package com.hnblc.blcwms.rest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hnblc.blcwms.client.service.HttpClientService;
import com.hnblc.blcwms.common.constant.StringPool;
import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.common.utils.WmsTimeUtils;
import com.hnblc.blcwms.common.utils.encrypt.BASE64;
import com.hnblc.blcwms.common.utils.encrypt.Base64Utils;
import com.hnblc.blcwms.common.utils.encrypt.MD5Util;
import com.hnblc.blcwms.common.utils.encrypt.RSAUtils;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpM;
import com.hnblc.blcwms.persistent.business.odata.service.IOdataExpMService;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;
import com.hnblc.blcwms.persistent.interfaces.business.dto.StockQuery;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientInfo;
import com.hnblc.blcwms.persistent.interfaces.client.service.IClientInfoService;
import com.hnblc.blcwms.persistent.interfaces.message.service.IMessageLogService;
import com.hnblc.blcwms.persistent.interfaces.sysConfig.entity.FieldDictionary;
import com.hnblc.blcwms.persistent.interfaces.sysConfig.service.IFieldDictionaryService;
import com.hnblc.blcwms.rest.dto.restInteraction.BaseRequest;
import com.hnblc.blcwms.rest.dto.restInteraction.EncryptRequest;
import com.hnblc.blcwms.rest.logistics.service.PDDService;
import com.hnblc.blcwms.rest.logistics.vo.PDDPrintData;
import com.hnblc.blcwms.rest.service.impl.LogisticsApplyService;
import com.hnblc.blcwms.serviceapi.api.dto.WareHouseOperate;
import com.hnblc.blcwms.serviceapi.api.dto.logistics.WaybillInfo;
import com.hnblc.blcwms.serviceapi.api.dto.ration.note.ExpressHead;
import com.hnblc.blcwms.serviceapi.api.dto.ration.status.ExpressStatusDto;
import org.apache.http.entity.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

//import com.hnblc.blcwms.persistent.business.odata.service.IOdataExpMService;
//import com.hnblc.blcwms.persistent.interfaces.business.entity.OrderCancel;
//import com.hnblc.blcwms.persistent.interfaces.business.service.IOrderCancelService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BlcWmsRestApplicationTests {

    @Autowired
    IClientInfoService clientInfoService;
//    @Autowired
//    IOrderCancelService orderCancelService;
    @Autowired
    IFieldDictionaryService fieldDictionaryService;

    @Autowired
    IMessageLogService messageLogService;
    @Autowired
    protected PlatformTransactionManager transactionManager;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    HttpClientService httpClientService;

//    @Autowired
//    IOdataExpMService odataExpMService;


    public static Logger logger = LoggerFactory.getLogger(BlcWmsRestApplicationTests.class);
    /**
     * 基本存取测试
     */
    @Test
    public void contextLoads() {
        ClientInfo ci  = new ClientInfo();
        ci.setClientKey("123123123");
        ci.setClientName("123123123123123");
        ci.setEnabled("1");
        ci.setEncrypt("0");


//        OrderCancel oc = new OrderCancel();
//        oc.setCancelClient("123123");
//        oc.setOrderNo("123123");
//        oc.setCancelReason("12312312");
//        oc.setMessageSeq("123123");
//        oc.setStatus("1");
////        oc.setCancelDate(new );
//        System.out.println(orderCancelService.save(oc));
    }

    /**
     * 手动插入字典表数据
     */
    @Test
    public void  testDicAdd(){
        List<FieldDictionary> saveList= new ArrayList<FieldDictionary>();
        String[][] array = {{"I_M_MESSAGE_LOG","PROCESS_STATUS","1","1","UNPROCESSED","未处理","接收报文保存成功，未进行处理。"},
                {"I_M_MESSAGE_LOG","PROCESS_STATUS","1","2","PROCESSED","已处理","接收报文处理成功。"},
                {"I_M_MESSAGE_LOG","PROCESS_STATUS","1","0","PROCESS_ERROR","处理失败","报文处理过程中出错，处理失败。"}};
        //取消订单状态
//        String[][] array = {{"I_B_EXP_LOG","STATUS","1","1","OPERATE_SUCCESS","操作成功","出货单操作成功。"},
//                {"I_B_EXP_LOG","STATUS","1","0","OPERATE_FAIL","操作失败","出货单操作失败。"},
//                {"I_B_EXP_LOG","OPERATE_TYPE","1","10","CREATE_EXP","创建出货单","创建出货单和包裹明细"},
//                {"I_B_EXP_LOG","OPERATE_TYPE","1","16","CANCEL_EXP","取消发货","取消出货单。"},
//                {"I_B_EXP_LOG","ERROR_CODE","1","11","NOT_FOUND","未找到","指定订单号的发货单不存在。"},
//                {"I_B_EXP_LOG","ERROR_CODE","1","12","WRONG_STATUS","不可取消","该出货单已经开始操作，不能通过接口取消。"},
//                {"I_B_EXP_LOG","ERROR_CODE","1","11","SEND","已发货","出货单已发货出库，无法取消。"}};
        for (int i = 0; i < array.length; i++) {
            FieldDictionary fieldDictionary = new FieldDictionary();
            fieldDictionary.setTableName(array[i][0]);
            fieldDictionary.setTableColumn(array[i][1]);
            fieldDictionary.setIsenabled(array[i][2]);
            fieldDictionary.setColumnValue(array[i][3]);
            fieldDictionary.setColumnText(array[i][4]);
            fieldDictionary.setColumnTextCn(array[i][5]);
            fieldDictionary.setValueDescription(array[i][6]);
            saveList.add(fieldDictionary);
        }
        fieldDictionaryService.saveBatch(saveList);
    }

    @Test
    public void testClientAdd() throws Exception {
        String clientName= "A05聚爱客户端";
        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setClientId(MD5Util.md5Encode(clientName+System.currentTimeMillis()));
        clientInfo.setClientName(clientName);
        clientInfo.setClientKey(MD5Util.md5Encode(String.valueOf(System.currentTimeMillis())));

        clientInfo.setEnabled("1");
        clientInfo.setEncrypt("0");
        Map keypair = RSAUtils.genKeyPair();
        clientInfo.setPrivateKey(RSAUtils.getPrivateKey(keypair));
        clientInfo.setPublicKey(RSAUtils.getPublicKey(keypair));
        clientInfoService.save(clientInfo);
    }
    /**
     * 缓存生效测试
     */
    @Test
    public void cacheTest(){
//        System.out.println(fieldDictionaryService.list());

//        fieldDictionaryService.ListLoadInCache("I_M_MESSAGE_LOG","PROCESS_STATUS").forEach(item -> {
//            System.out.println(item.getTableName().length());
//            System.out.println(item.getTableColumn().length());
//        });
        System.out.println(fieldDictionaryService.listLoadInCache("I_M_MESSAGE_LOG","PROCESS_STATUS").size());//.forEach(item -> System.out.println(item.getColumnTextCn()));
        return;
    }

    /**
     * 客户端http请求测试
     * @throws IOException
     */
    @Test
    public void httpClientPushbackTest() throws Exception {
//        String url = "http://testoms.javacoding.cn/index.php/openapi/rpc/service/";
        String url = "http://test-homs.hhb123.cn/index.php/openapi/rpc/service";
        String token = "kKNRhdoEhxbYEBgCXJiZGCKDBRULZTTP";
        Map<String,String> param = new TreeMap<>();
        param.put("method","wms.orderStatus");
        param.put("flag","wms");
        param.put("sourceexp_no","19050500000006");
       // param.put("type","json");
       // param.put("charset","utf-8");
        param.put("status","1");
        String timeStamp = new Long(System.currentTimeMillis()).toString();
        param.put("timestamp",timeStamp.substring(0,timeStamp.length()-3));
        Set<String> keySet = param.keySet();
        Iterator<String> iterator = keySet.iterator();
        StringBuilder sign = new StringBuilder();
        while (iterator.hasNext()){
            final String key = iterator.next();
            sign.append(key);
            sign.append(param.get(key));
        }
        param.put("sign", MD5Util.md5Encode(MD5Util.md5Encode(sign.toString()).toUpperCase()+token).toUpperCase());

        System.out.println(
                httpClientService.sendHttpPost(url,param)
        );
    }


    /**
     * 客户端http请求BODY测试
     * @throws IOException
     */
    @Test
    public void httpClientOrderTest() throws Exception {
//        String url = "http://testoms.javacoding.cn/index.php/openapi/rpc/service/";
        String url = "http://localhost:8090/blc/wms/api/order/create";
        String timestamp = new Long(Timestamp.valueOf(LocalDateTime.now()).getTime()).toString();
        String key = "a991b10fa22c4dade81c61387c3906db";
        String clientId = "1c7c6decb65785856eaea7b1a6241899";

        WareHouseOperate<ExpressHead> bdata = new WareHouseOperate<>();
        bdata.setEnterpriseNo("8888");
        bdata.setWarehouseNo("A05");
        bdata.setOwnerNo("JASMZ");
        bdata.setCustomNo("JASMZ");
//        bdata.setCustomNo("4101W68100");
        ExpressHead expressHead = new ExpressHead();
        expressHead.setOrderSource("1124124");
        expressHead.setExpType("4");
        expressHead.setSourceExpNo("121412412");

        bdata.setOperateDetails(expressHead);

//        String businessBase = this.objectMapper.writeValueAsString(bdata);

        String test = "{\n" +
                "    \"operate_details\": {\n" +
                "        \"order_details\": [\n" +
                "            {\n" +
                "                \"owner_article_no\": \"p0000001341\",\n" +
                "                \"article_identifier\": \"\",\n" +
                "                \"barcode\": \"p0000001341\",\n" +
                "                \"item_type\": \"0\",\n" +
                "                \"packing_qty\": 1,\n" +
                "                \"unit_cost\": \"1578.00\",\n" +
                "                \"plan_qty\": 1\n" +
                "            }\n" +
                "        ],\n" +
                "        \"source_exp_no\": \"20200109105713882791\",\n" +
                "        \"exp_type\": \"4\",\n" +
                "        \"customs_inspection\": \"0\",\n" +
                "        \"receiver\": \"陈元元\",\n" +
                "        \"receive_province\": \"安徽省\",\n" +
                "        \"receive_city\": \"蚌埠市\",\n" +
                "        \"receive_zone\": \"淮上区\",\n" +
                "        \"receive_address\": \"槐花园C区北门七佳母婴\",\n" +
                "        \"receive_mobile_no\": \"13909626066\",\n" +
                "        \"take_type\": \"0\",\n" +
                "        \"operate\": \"1\",\n" +
                "        \"good_count\": 1,\n" +
                "        \"sku_count\": 1,\n" +
                "        \"exp_remark\": \"包好一点\",\n" +
                "        \"shipper\": \"于尧\",\n" +
                "        \"shipper_company_name\": \"郑州火牛商贸有限公司\",\n" +
                "        \"shipper_province\": \"河南省\",\n" +
                "        \"shipper_city\": \"郑州市\",\n" +
                "        \"shipper_zone\": \"金水区\",\n" +
                "        \"shipper_address\": \"农业路沙口路302仓库\",\n" +
                "        \"shipper_postcode\": \"450000\",\n" +
                "        \"shipper_mobile_no\": \"18838239961\"\n" +
                "    },\n" +
                "    \"enterprise_no\": \"8888\",\n" +
                "    \"warehouse_no\": \"A05\",\n" +
                "    \"owner_no\": \"JASMZ\",\n" +
                "    \"custom_no\": \"JASMZ\"\n" +
                "}";

        System.out.println(test);
        System.out.println(Base64Utils.encode(test.getBytes(StringPool.CHARSET_UTF8)));
        EncryptRequest encryptRequest = new EncryptRequest();
        encryptRequest.setTimestamp(timestamp);
        encryptRequest.setVersion("1.0");
        encryptRequest.setClientId(clientId);
        encryptRequest.setBusinessData(Base64Utils.encode(test.getBytes(StringPool.CHARSET_UTF8)));
        encryptRequest.setSignature(MD5Util.md5Encode(encryptRequest.getBusinessData()+timestamp+key));
        System.out.println(this.objectMapper.writeValueAsString(encryptRequest));
        System.out.println(
                httpClientService.sendHttpPost(url,this.objectMapper.writeValueAsString(encryptRequest), ContentType.APPLICATION_JSON)
        );
    }
    /**
     * 客户端http请求BODY测试
     * @throws IOException
     */
    @Test
    public void httpClientStockTest() throws Exception {
//        String url = "http://testoms.javacoding.cn/index.php/openapi/rpc/service/";
        String url = "http://localhost:8090/blc/wms/log/stock/salable";
        String timestamp = "1321834313101";
        String key = "cd20430ff81503da16c9946d0e57169a";
        String clientId = "424b51ee165c4a588533847e2346d9fd";

        WareHouseOperate<ExpressHead> bdata = new WareHouseOperate<>();
        bdata.setEnterpriseNo("8888");
        bdata.setWarehouseNo("001");
        bdata.setOwnerNo("4101W68100");
        bdata.setCustomNo("4101W68100");
        List<StockQuery> skuList = new ArrayList<>();
        ExpressHead expressHead = new ExpressHead();
        expressHead.setOrderSource("");
        expressHead.setExpType("B2C");
        expressHead.setSourceExpNo("121412412");
        bdata.setOperateDetails(expressHead);

        String businessBase = this.objectMapper.writeValueAsString(bdata);
        System.out.println(businessBase);
        System.out.println(Base64Utils.encode(businessBase.getBytes(StringPool.CHARSET_UTF8)));
        EncryptRequest encryptRequest = new EncryptRequest();
        encryptRequest.setTimestamp(timestamp);
        encryptRequest.setVersion("1.0");
        encryptRequest.setClientId(clientId);
        encryptRequest.setBusinessData(Base64Utils.encode(businessBase.getBytes(StringPool.CHARSET_UTF8)));
        encryptRequest.setSignature(MD5Util.md5Encode(encryptRequest.getBusinessData()+timestamp+key));
        System.out.println(this.objectMapper.writeValueAsString(encryptRequest));
        System.out.println(
                httpClientService.sendHttpPost(url,this.objectMapper.writeValueAsString(encryptRequest), ContentType.APPLICATION_JSON)
        );
    }

    public void testPush1(){

    }

    @Test
    public void testOrderReturn() throws Exception {

        String result;
        String url = "http://171.8.68.14:7280/e-channel-entry/deliveryInfo/receivedDeliveryInfo";  //通关服务平台区外地址
//        String url = "http://172.30.32.31:8080/e-channel-entry/deliveryInfo/receivedDeliveryInfo";
//        String url = "http://1.192.212.152:8180/ocbp/orderExtension/syncStatus";   //昂捷测试反馈接口
//        String url = "http://36.99.44.38:8180/ocbp/orderExtension/syncStatus";   //昂捷正式反馈接口
//        String url = "http://5kdws2.natappfree.cc/ocbp/orderExtension/syncStatus";
        EncryptRequest request = new EncryptRequest();
        String clientKey = "8d6b6cc09e5c90a5fa302054cdb343f9"; //区内仓key
//        String clientKey = "cfa33ca9f4939a54f92b907c57b64e7c"; //区外仓key
       /* String[][] array = {{"119102881635","A005360722964"}
        };*/
        String[][] array = {
                {"23523535235","235235235235235"},
                {"235235235","235235235355"}
        };
        List<ExpressStatusDto> businessData = new ArrayList<>();
        for (String[] a :array){
            ExpressStatusDto statusDto = new ExpressStatusDto();
//            statusDto.setOwnerNo("ZTMDS");
//            statusDto.setOwnerNo("ZDMZZ");
//            statusDto.setOwnerNo("4101W68100");
            statusDto.setOrderNo(a[0]);
            statusDto.setLogisticNo(a[1]);
            statusDto.setStatus("3");
            businessData.add(statusDto);
        }
        ExpressStatusDto statusDto1 = new ExpressStatusDto();
        statusDto1.setOwnerNo("ZTMDS");
        statusDto1.setOrderNo("119091602877");
        statusDto1.setLogisticNo("A005313972322");
        statusDto1.setStatus("3");
//        businessData.add(statusDto1);
        request.setClientId("a975124fb4ff691a1e17c73830e21721");  //区内辅助平台ID
//        request.setClientId("76e0d172be4a8ce746cab6ab0ce64c3e");   //区外昂捷erpID
        request.setTimestamp(new Long(WmsTimeUtils.datatimeToTimestamp(LocalDateTime.now())).toString());
        request.setVersion("1.0");

        System.out.println(objectMapper.writeValueAsString(businessData));
        String businessDaStr = BASE64.encryptBASE64(objectMapper.writeValueAsString(businessData).getBytes(StringPool.CHARSET_UTF8));
        request.setBusinessData(businessDaStr);
        System.out.println(businessDaStr);
        request.setSignature(MD5Util.md5Encode(businessDaStr+request.getTimestamp()+clientKey));
        String param = objectMapper.writeValueAsString(request);

        System.out.println(param);
        result = httpClientService.sendHttpPost(url,param, ContentType.APPLICATION_JSON);
        System.out.println(result);
    }
    /**
     * 获取路径测试
     * @throws IOException
     */
    @Test
    public void getProjectPath() throws IOException {

        // 第一种：获取类加载的根路径   D:\git\daotie\daotie\target\classes
        File f = new File(this.getClass().getResource("/").getPath());
        System.out.println(f);

        // 获取当前类的所在工程路径; 如果不加“/”  获取当前类的加载目录  D:\git\daotie\daotie\target\classes\my
        File f2 = new File(this.getClass().getResource("").getPath());
        System.out.println(f2);

        // 第二种：获取项目路径    D:\git\daotie\daotie
        File directory = new File("");// 参数为空
        String courseFile = directory.getCanonicalPath();
        System.out.println(courseFile);


        // 第三种：  file:/D:/git/daotie/daotie/target/classes/
        URL xmlpath = this.getClass().getClassLoader().getResource("");
        System.out.println(xmlpath);


        // 第四种： D:\git\daotie\daotie
        System.out.println(System.getProperty("user.dir"));
        /*
         * 结果： C:\Documents and Settings\Administrator\workspace\projectName
         * 获取当前工程路径
         */

        // 第五种：  获取所有的类路径 包括jar包的路径
        System.out.println(System.getProperty("java.class.path"));
    }

    /**
     * 日志级别分文件测试
     */
    @Test
    public void logTest(){
        Logger logger = LoggerFactory.getLogger(this.getClass());

        logger.debug("DEBUG");
        logger.error("ERROR");
        logger.info("INFO");
        logger.trace("TRACE");
        logger.warn("WARN");
    }

    @Autowired
    IOdataExpMService odataExpMService;

    @Test
//    @Transactional
    public void multiDb() throws Exception {

        logger.info("Load with transaction manager: {}", transactionManager.getClass().getSimpleName());
        clientInfoService.getOne(new QueryWrapper<ClientInfo>().eq("CLIENT_KEY","liwefnwoinvwoivwandfon"));
//        this.testClientAdd("test1");
        logger.info("second transaction manager: {}", transactionManager.getClass().getSimpleName());
        OdataExpM oem = odataExpMService.getOne(new QueryWrapper<OdataExpM>().eq("exp_no","OE00319030500001"));
        messageLogService.list();
        oem.setStatus("16");
        odataExpMService.saveOrUpdate(oem);
        logger.info("third transaction manager: {}", transactionManager.getClass().getSimpleName());

    }

    @Test
    public void lombokTest(){
        BaseRequest br  = new BaseRequest();
//        br.setClientKey("123123213");
        System.out.println(br.toString());
    }

    @Test
    public void resTest(){
//        RestResponse testre = new RestResponse(new Response<Boolean>(RationNoteResultEnum.CREATE_SUCCESS));
//        System.out.println(testre.getResultMsg());
        System.out.println("11111");
    }


    @Autowired
    LogisticsApplyService logisticsApplyService;
    @Test
    public void pddTest() throws JsonProcessingException {
        ExpressQueryVO queryVO = new ExpressQueryVO();
        queryVO.setExpNo("OE00319120503924");
        //YT2027840007122 446-200-38-001测试单原运单号和三段码
        queryVO.setWarehouseNo("003");
//        pinduoduoService.generateToken();
//        pinduoduoService.refresToken();
        Response<List<WaybillInfo>> response = logisticsApplyService.applyWaybillInfo(queryVO,"PDD_YTO");
        System.out.println(objectMapper.writeValueAsString(response));

    }

    @Autowired
    PDDService PDDService;
    @Test
    public void pddCancelTest(){





                String s1 = "YTO";
        String s2 = "YT9073088014072";

        String[] a = {"YT9077844131500",
                "YT9077844131239",
                "YT9077844069543",
                "YT9077844105602",
                "YT9077844135215",
                "YT9077844142466",
                "YT9077844060872",
                "YT9077844069200",
                "YT9077844115815",
                "YT9077844069374",
                "YT9077844131445",
                "YT9077844110283",
                "YT9077844109857",
                "YT9077844131651",
                "YT9077844069051",
                "YT9077844135900"};

        for (int i = 0; i < a.length; i++) {

            Response<Boolean> response = PDDService.cancelWaybillNo(s1,a[i]);
            System.out.println(response.getCode());
            System.out.println(response.getMessage());
            System.out.println(response.getErrorCode());
            System.out.println(response.getFailedMessage());
        }
    }


    public static void main(String[] args) throws IOException {
        String s = "{\n" +
                "    \"recipient\": {\n" +
                "        \"address\": {\n" +
                "            \"city\": \"徐州市\",\n" +
                "            \"detail\": \"江苏省徐州市铜山区大许镇条筋六组25号\",\n" +
                "            \"district\": \"铜山区\",\n" +
                "            \"province\": \"江苏省\",\n" +
                "            \"town\": \"铜山区\"\n" +
                "        },\n" +
                "        \"name\": \"康治先\",\n" +
                "        \"phone\": \"18626046616\"\n" +
                "    },\n" +
                "    \"routingInfo\": {\n" +
                "        \"bigShotCode\": \"446徐州\",\n" +
                "        \"bigShotName\": \"446徐州\",\n" +
                "        \"endBranchCode\": \"516001\",\n" +
                "        \"endBranchName\": \"\",\n" +
                "        \"objectId\": 1,\n" +
                "        \"requestId\": 639447753371369500,\n" +
                "        \"threeSegmentCode\": \"446-200-38-001\",\n" +
                "        \"threeSegmentCodeSource\": \"2\"\n" +
                "    },\n" +
                "    \"sender\": {\n" +
                "        \"address\": {\n" +
                "            \"city\": \"郑州市\",\n" +
                "            \"detail\": \"航海东路1508号 河南保税物流中心\",\n" +
                "            \"district\": \"管城回族区\",\n" +
                "            \"province\": \"河南省\",\n" +
                "            \"town\": \"管城回族区\"\n" +
                "        },\n" +
                "        \"mobile\": \"4008006785\",\n" +
                "        \"name\": \"拼多多食品旗舰店\",\n" +
                "        \"phone\": \"0371-56589111\"\n" +
                "    },\n" +
                "    \"signature\": \"71D4879CF9980751A1634940B98D37B0\",\n" +
                "    \"templateUrl\": \"http://pinduoduoimg.yangkeduo.com/print_template/2019-03-18/40ed4035a27f0039f72401d1f5cefa0d.xml\",\n" +
                "    \"waybillCode\": \"YT9073088014072\",\n" +
                "    \"wpCode\": \"YTO\"\n" +
                "}";
        ObjectMapper objectMapper1 = new ObjectMapper();
//        objectMapper1.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
//        objectMapper1.setDefaultPropertyInclusion(JsonInclude.Include.NON_EMPTY);
//        objectMapper1.setConfig(new DeserializationConfig());
        PDDPrintData testObj = objectMapper1.readValue(s, new TypeReference<PDDPrintData>() {
        });

        System.out.println(testObj);
    }
}

