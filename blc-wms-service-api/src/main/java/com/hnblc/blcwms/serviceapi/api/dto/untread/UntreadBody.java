package com.hnblc.blcwms.serviceapi.api.dto.untread;

import com.hnblc.blcwms.serviceapi.api.group.custom.express.UntreadGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @ClassName: UntreadBody
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/11/11
 * @Version: V1.0
 */
@Data
public class UntreadBody {
    @Length(max = 20)
    @NotBlank(groups = {UntreadGroup.class})
    private String ownerArticleNo;
    private String barCode;
    private Long itemIndex;
    @Min(1)
    @NotNull(groups = {UntreadGroup.class})
    private Double packageQty;
    @Min(1)
    @NotNull(groups = {UntreadGroup.class})
    private Double planQty;

    private String errorReason;
}
