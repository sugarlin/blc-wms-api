package com.hnblc.blcwms.serviceapi.odata.constant;

import java.util.HashMap;

public class ExpressConst {

    /**
     * 业务字典表常量
     */
    //出货单状态
    public final static String ODATA_EXP_M__STATUS__UNREADY="-1";
    public final static String ODATA_EXP_M__STATUS__READY="10";
    public final static String ODATA_EXP_M__STATUS__ONBOARD="11";
    public final static String ODATA_EXP_M__STATUS__LOCATED="12";
    public final static String ODATA_EXP_M__STATUS__SEND="13";
    public final static String ODATA_EXP_M__STATUS__CANCELED="16";
    //出货单明细状态
    public final static String ODATA_EXP_M__EXP_STATUS__READY="00";
    public final static String ODATA_EXP_M__EXP_STATUS__LOCATED="05";
    public final static String ODATA_EXP_M__EXP_STATUS__PICKED="10";
    public final static String ODATA_EXP_M__EXP_STATUS__INNER_CHECKED="20";
    public final static String ODATA_EXP_M__EXP_STATUS__OUTTER_CHECKED="30";
    public final static String ODATA_EXP_M__EXP_STATUS__LOADED_VEHICLE="50";
    public final static String ODATA_EXP_M__EXP_STATUS__SEAL_UP="70";
    public final static String ODATA_EXP_M__EXP_STATUS__CANCELED="90";


    public final static String ODATA_EXP_D__STATUS__READY="10";


    public final static String ODATA_EXP_M__EXP_TYPE__OE="OE";
    public final static String ODATA_EXP_M__EXP_TYPE__ID="ID";
    public final static String ODATA_EXP_M__EXP_TYPE__B2C="B2C";
    public final static String ODATA_EXP_M__EXP_TYPE__JH="JH";
    public final static String ODATA_EXP_M__EXP_TYPE__OHQ="OHQ"; //账册结转出区
    public final static String ODATA_EXP_M__EXP_TYPE__OYP="OYP"; //样品担保出区
    public final static String ODATA_EXP_M__EXP_TYPE__B2B="B2B"; //B2B出货


    public final static HashMap<String,String> EXP_TYPE_CODE_MAP= new HashMap<>();

    static {
        EXP_TYPE_CODE_MAP.put("0",ODATA_EXP_M__EXP_TYPE__OE);
        EXP_TYPE_CODE_MAP.put("1",ODATA_EXP_M__EXP_TYPE__ID);
        EXP_TYPE_CODE_MAP.put("2",ODATA_EXP_M__EXP_TYPE__B2C);
        EXP_TYPE_CODE_MAP.put("3",ODATA_EXP_M__EXP_TYPE__JH);
        EXP_TYPE_CODE_MAP.put("4",ODATA_EXP_M__EXP_TYPE__OHQ);
        EXP_TYPE_CODE_MAP.put("5",ODATA_EXP_M__EXP_TYPE__OYP);
        EXP_TYPE_CODE_MAP.put("6",ODATA_EXP_M__EXP_TYPE__B2B);
    }
}
