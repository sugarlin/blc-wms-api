package com.hnblc.blcwms.serviceapi.definition.constant;

public class BaseDefineConst {


    public static final int KIND_OWNER = 0;
    public static final int KIND_CUST = 1;
    public static final int KIND_SHIPPER = 2;
    public static final int KIND_ARTICLE = 3;

    public static final String SHIPPER_ENABLE_QRCODE="1";
}
