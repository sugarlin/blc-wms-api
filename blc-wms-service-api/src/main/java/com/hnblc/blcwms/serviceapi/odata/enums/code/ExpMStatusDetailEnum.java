package com.hnblc.blcwms.serviceapi.odata.enums.code;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum ExpMStatusDetailEnum implements BaseEnums {



    READY("00","建单","EXPRESS_READY"),
    LOCATED("05","已定位","EXPRESS_LOCATED"),
    PICKED("10","已拣货","EXPRESS_ONBOARD"),
    INNER_CHECKED("20","内复核完成","EXPRESS_SENT"),
    OUTTER_CHECKED("30","外复核完成","EXPRESS_LOCATED"),
    LOADED_VEHICLE("50","已装车","EXPRESS_SENT"),
    SEAL_UP("70","已定位","EXPRESS_LOCATED"),
    CANCELLEDD("90","已取消","EXPRESS_CANCELED");


    private String code;
    private String textCn;
    private String testEn;

    ExpMStatusDetailEnum(String code, String textCn, String testEn){
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }


    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (ExpMStatusDetailEnum enums : EnumSet.allOf(ExpMStatusDetailEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
