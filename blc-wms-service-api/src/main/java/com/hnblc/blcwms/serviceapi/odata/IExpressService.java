package com.hnblc.blcwms.serviceapi.odata;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpD;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpM;
import com.hnblc.blcwms.persistent.business.odata.vo.TraceInfo;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.serviceapi.api.dto.ration.note.ExpressHead;

import java.util.Collection;
import java.util.List;

public interface IExpressService{
    /**
     * 取消出货单
     * @param odataExpM
     * @return
     */
    boolean cancelExpress(OdataExpM odataExpM);

    /**
     * 创建出货单
     * @param expressHead 出货单对象
     * @return
     */
    Response<Boolean> createExpress(ExpressHead expressHead, ClientAuth clientAuth);

    /**
     * 查找出货包裹内件商品库位变动明细
     * @param odataExpNo
     * @return
     */
    List<TraceInfo> traceExpressGoods(String odataExpNo);
    /**
     * 保存出货单
     * @param odataExpM
     * @param detailList
     * @return
     */
    boolean saveExpress(OdataExpM odataExpM, Collection<OdataExpD> detailList);

}
