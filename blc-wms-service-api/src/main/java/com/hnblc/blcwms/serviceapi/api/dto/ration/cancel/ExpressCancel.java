package com.hnblc.blcwms.serviceapi.api.dto.ration.cancel;

import com.hnblc.blcwms.common.base.BaseDto;
import com.hnblc.blcwms.serviceapi.api.group.custom.express.ExpCancelGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ExpressCancel extends BaseDto implements Serializable{

    private static final long serialVersionUID = 7699806558287254700L;
    @ApiModelProperty(name = "订单号",example = "114212124121212",position = 1)
    @NotEmpty(message = "{validation.request.commonField.notEmpty}",groups = {ExpCancelGroup.class})
    private String orderNo;
    @ApiModelProperty(name = "订单取消原因",example = "客户取消订单",position = 30)
    @NotEmpty(message = "{validation.request.commonField.notEmpty}",groups = {ExpCancelGroup.class})
    private String cancelReason;
    @ApiModelProperty(hidden = true)
    private String operateStatus;
    @ApiModelProperty(hidden = true)
    private String cancelResult;
    @ApiModelProperty(hidden = true)
    private String createResult;
}
