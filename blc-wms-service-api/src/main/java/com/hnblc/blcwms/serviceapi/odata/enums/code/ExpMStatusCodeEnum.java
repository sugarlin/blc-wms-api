package com.hnblc.blcwms.serviceapi.odata.enums.code;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum ExpMStatusCodeEnum implements BaseEnums {



    UNREADY("-1","未处理","EXPRESS_UNREADY"),
    READY("10","建单","EXPRESS_READY"),
    ONBOARD("11","组板","EXPRESS_ONBOARD"),
    LOCATED("12","已定位","EXPRESS_LOCATED"),
    SENT("13","结案","EXPRESS_SENT"),
    CANCELLEDD("16","已取消","EXPRESS_CANCELED");


    private String code;
    private String textCn;
    private String testEn;

    ExpMStatusCodeEnum(String code, String textCn, String testEn){
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }


    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (ExpMStatusCodeEnum enums : EnumSet.allOf(ExpMStatusCodeEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
