package com.hnblc.blcwms.serviceapi.api.dto.definition;

import com.hnblc.blcwms.serviceapi.api.group.owner.definition.DefCategoryGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
public class ArticleCategory {
    @ApiModelProperty(name = "类别代码",position = 1)
    @NotBlank(groups = {DefCategoryGroup.class})
    @Length(max=20)
    private String categoryCode;
    @ApiModelProperty(name = "类别名称",position = 2)
    @NotBlank(groups = {DefCategoryGroup.class})
    @Length(max=45)
    private String categoryName;
    @Valid
    private List<ArticleCategory> categories;
    private String errorReason;
}
