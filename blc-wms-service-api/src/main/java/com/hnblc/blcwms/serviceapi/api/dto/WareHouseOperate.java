package com.hnblc.blcwms.serviceapi.api.dto;

import com.hnblc.blcwms.serviceapi.api.group.BaseGroup;
import com.hnblc.blcwms.serviceapi.api.group.custom.CustomsGroup;
import com.hnblc.blcwms.serviceapi.api.group.owner.stock.StockOwnerGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Validated
public class WareHouseOperate<T> {
    @ApiModelProperty(name = "仓储企业代码",example = "8888",position = 1)
    @NotBlank(groups = {BaseGroup.class})
    @Length(max = 5)
    private String enterpriseNo;
    @ApiModelProperty(name = "所在仓别",example = "001",position = 2)
    @NotBlank(groups = {CustomsGroup.class, StockOwnerGroup.class})
    @Length(max = 5)
    private String warehouseNo;
    @ApiModelProperty(name = "仓储货主代码",example = "ZDMZZ",position = 3)
    @NotBlank(groups = {BaseGroup.class})
    @Length(max = 10)
    private String ownerNo;
    @ApiModelProperty(name = "仓储电商客户代码",example = "ZDMZZ",position = 4)
    @NotBlank(groups = {CustomsGroup.class})
    @Length(max = 10)
    private String customNo;
    @ApiModelProperty(name = "具体请求操作",position = 5)
    @Valid
    @NotNull(groups = {BaseGroup.class})
    private T operateDetails;

}
