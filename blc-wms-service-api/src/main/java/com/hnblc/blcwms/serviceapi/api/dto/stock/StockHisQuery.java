package com.hnblc.blcwms.serviceapi.api.dto.stock;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Validated
public class StockHisQuery {


    @ApiModelProperty(name = "门店代码",position = 5,example = "ZDMZZ")
    @NotEmpty(message = "{validation.request.commonField.notEmpty}")
    private String storeCode;

    @ApiModelProperty(name = "本次查询日期",position = 5,example = "2019-07-04")
    @NotEmpty(message = "{validation.request.commonField.notEmpty}")
    private String endDate;

}
