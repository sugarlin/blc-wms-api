package com.hnblc.blcwms.serviceapi.api.enums.result.businiss;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum ExpErpStatusResultEnum implements BaseResultEnums {



    CREATE_SUCCESS(true,"10","回传成功","UNKNOWN_SHIPPER"),
    CREATE_ERROR_ORDER_NOT_EXIST(false,"11","订单不存在","UNKNOWN_SHIPPER"),
    CREATE_ERROR_REPEAT_SENT(false,"12","订单已回传","UNKNOWN_SKU"),
    CREATE_ERROR_SENT_FAIL(false,"99","回传时发生未知错误：{0}","ORDER_EXIST");


    private String code;
    private String category="61";  //订单出货类型错误
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    ExpErpStatusResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (ExpErpStatusResultEnum enums : EnumSet.allOf(ExpErpStatusResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
