package com.hnblc.blcwms.serviceapi.api.dto.ration.note;

import com.hnblc.blcwms.serviceapi.api.group.custom.express.ExpGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ExpressBody {


    /**
     * 商品编码
     */
    @ApiModelProperty(name = "商品编码",example = "156168118181",position = 1)
    @Length(max = 30)
    @NotEmpty(groups = {ExpGroup.class})
    private String ownerArticleNo;
    /**
     * 商品备案号
     */
    @ApiModelProperty(name = "商品备案号",example = "1561846831861",position = 2)
    @Length(max = 50)
    @NotEmpty(groups = {ExpGroup.class})
    private String articleIdentifier;

    /**
     * 商品主条码
     */
    @ApiModelProperty(name = "商品主条码",example = "156168118181",position = 3)
    @Length(max = 50)
    @NotEmpty(groups = {ExpGroup.class})
    private String barcode;
    /**
     * 订单商品类型：0-普通；1-赠品

     */
    @ApiModelProperty(name = "订单商品类型：0-普通；1-赠品",example = "0",position = 4)
    @Length(max = 1)
    @NotEmpty(groups = {ExpGroup.class})
    private String itemType;
    /**
     * 包装数量
     */
    @ApiModelProperty(name = "包装数量,默认为1",example = "1",position = 5)
    @NotNull(groups = {ExpGroup.class})
    private Double packingQty;

    /**
     * 出货单价
     */
    @ApiModelProperty(name = "出货单价",example = "1255",position = 6)
    @NotNull(groups = {ExpGroup.class})
    private Double unitCost;

    /**
     * 预定出货数量
     */
    @ApiModelProperty(name = "预定出货数量",example = "2",position = 7)
    @NotNull(groups = {ExpGroup.class})
    private Double planQty;
}
