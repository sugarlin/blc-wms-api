package com.hnblc.blcwms.serviceapi.api.enums.result.log;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum MessageLogResultEnum implements BaseResultEnums {



    UNPROCESSED(false,"1","未处理","UNPROCESSED"),
    PROCESSED(true,"2","处理完成","PROCESSED");



    private boolean success;
    private String code;
    private String category="31";
    private String textCn;
    private String testEn;

    MessageLogResultEnum(boolean success,String code, String textCn, String testEn){
        this.success = success;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (MessageLogResultEnum enums : EnumSet.allOf(MessageLogResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
