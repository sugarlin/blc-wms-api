package com.hnblc.blcwms.serviceapi.api.enums.code;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum DefinitionLogTypeEnum implements BaseEnums {



    OWNER("1","货主","OWNER"),
    CUSTOM("2","客户","CUSTOM"),
    SUPPLIER("3","供应商","SUPPLIER"),
    ARTICLE_CATEGORY("4","商品类别","ARTICLE_CATEGORY"),
    ARTICLE("5","商品信息","ARTICLE"),
    SHIPPER("6","承运商","SHIPPER"),
    OWNER_WITH_CUSTOM("7","货主兼客户","OWNER_WITH_CUSTOM");


    private String code;
    private String textCn;
    private String testEn;

    DefinitionLogTypeEnum(String code, String textCn, String testEn){
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }


    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (DefinitionLogTypeEnum enums : EnumSet.allOf(DefinitionLogTypeEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
