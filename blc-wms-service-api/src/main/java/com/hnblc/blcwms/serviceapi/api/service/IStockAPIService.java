package com.hnblc.blcwms.serviceapi.api.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.business.stock.dto.StockHisQuery;
import com.hnblc.blcwms.persistent.business.stock.entity.HisStock;
import com.hnblc.blcwms.persistent.interfaces.business.dto.StockParam;
import com.hnblc.blcwms.persistent.interfaces.business.entity.SalableStock;

import java.util.List;

public interface IStockAPIService {

    List<HisStock> listHistoryStock(StockHisQuery stockHisQuery);

    Response<List<SalableStock>> getSalableStocks(StockParam stockParam);
}
