package com.hnblc.blcwms.serviceapi.api.enums.result.businiss;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum RationNoteResultEnum implements BaseResultEnums {



    SUCCESS(true,"10","保存成功","SUCCESS"),
    ERROR_UNKNOWN_SHIPPER(false,"11","承运商代码有误","UNKNOWN_SHIPPER"),
    ERROR_UNKNOWN_SKU(false,"12","订单内商品代码缺少基础信息：{0}","UNKNOWN_SKU"),
    ERROR_SKU_COUNT(false,"13","订单商品种类数与收到详细信息不一致","SKU_COUNT_ERROR"),
    ERROR_GOOD_COUNT(false,"14","订单商品总数量与详细信息不一致","GOOD_COUNT_ERROR"),
    ERROR_UNKNOWN_TYPE(false,"15","未知的出货类型","UNKNOWN_TYPE"),
    ERROR_EXISTED(false,"16","该订单已存在","ORDER_EXIST"),
    ERROR_SAVE_FAIL(false,"17","保存时发生未知错误：{0}","SAVE_FAIL"),

    NOTE_ERROR_EXISTED(false,"18","该出货通知单已存在接口缓冲区，请等待处理","ORDER_EXIST");


    private String code;
    private String category="13";  //订单出货类型错误
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    RationNoteResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (RationNoteResultEnum enums : EnumSet.allOf(RationNoteResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
