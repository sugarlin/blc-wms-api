package com.hnblc.blcwms.serviceapi.api.enums.result;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hnblc.blcwms.common.base.BaseResultEnums;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum APIResultEnum implements BaseResultEnums {



    SUCCESS(true,"10","处理成功","SUCCESS"),
    ERROR_JSON_FORMAT(false,"11","报文格式转换错误，JSON/XML结构异常","BAD FORMAT OF JSON/XML"),
    ERROR_DECODE_BASE64(false,"12","业务数据解码失败","BUSINESS DATA BASE64 DECODE FAIL"),
    ERROR_CLIENT_NOT_FOUND(false,"13","客户端未注册","CLIENT NOT EXIST"),
    ERROR_SIGNATURE_FAIL(false,"14","验签失败","SIGNATURE VALIDATION FAIL"),
    ERROR_UNKNOWN(false,"99","未知错误","UNKNOWN");


    private String code;
    private String category="10";
    private Boolean isSuccess;
    private String textCn;
    private String testEn;

    APIResultEnum(boolean isSuccess, String code, String textCn, String testEn){
        this.isSuccess = isSuccess;
        this.code = code;
        this.textCn = textCn;
        this.testEn = testEn;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String category(){
        return this.category;
    }

    @Override
    public boolean isSuccess(){
        return this.isSuccess;
    }

    @Override
    public String getMessage(){
        return this.getLocaleMessage(Locale.getDefault());
    }


    public String getLocaleMessage(Locale locale) {
        if (Locale.CHINA.equals(locale))
        return this.textCn;
        else return this.testEn;
    }

    private static final List<Map<String, Object>> epte = Lists.newArrayList();

    static {
        for (APIResultEnum enums : EnumSet.allOf(APIResultEnum.class)) {
            Map<String, Object> lookup = Maps.newHashMap();
            lookup.put("code", enums.code());
            lookup.put("textCn", enums.toString());
            lookup.put("testEn", enums.toString());
            epte.add(lookup);
        }
    }

    public static List<Map<String, Object>> statusList() {
        return epte;
    }

}
