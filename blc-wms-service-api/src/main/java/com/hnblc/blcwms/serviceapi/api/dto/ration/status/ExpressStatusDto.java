package com.hnblc.blcwms.serviceapi.api.dto.ration.status;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * @ClassName: ExpressStatusDto
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/8/9
 * @Version: V1.0
 */
@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ExpressStatusDto {
    private String ownerNo;
    private String customNo;
    private String orderNo;
    private String logisticNo;
    private String deliverNo;
    private String loadProposeNo;
    private String sendName;
    private LocalDateTime operateDate;
    private String status;
}
