package com.hnblc.blcwms.serviceapi.api.service;

import com.hnblc.blcwms.common.interaction.Response;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.serviceapi.api.dto.untread.UntreadBody;
import com.hnblc.blcwms.serviceapi.api.dto.untread.UntreadHead;

/**
 * @since 2019-11-20
 * 退配通知单（出库订单退货）服务接口
 * @author linsong
 */
public interface IUntreadAPIService {

    /**
     * 创建订单退货通知单
     * @param untreadHead 退货通知单
     * @param clientAuth 授权信息
     * @return Response<UntreadBody> 处理结果
     */
    Response<UntreadBody> createReturnNote(UntreadHead untreadHead, ClientAuth clientAuth);
}
