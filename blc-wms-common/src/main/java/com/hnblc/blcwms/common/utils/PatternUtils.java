package com.hnblc.blcwms.common.utils;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternUtils {

    private PatternUtils(){}

    public static boolean isNumber(String numStr,int digit){
        Pattern pattern = Pattern.compile(String.format("^\\d{%d}$", digit));
        Matcher isNum = pattern.matcher(numStr);
        if (isNum.matches())return true;
        else return false;
    }

    /**
     * 判断是否合法时间戳
     * @param timestamp
     * @return
     */
    public static boolean isTimeStamp(String timestamp){
        return isNumber(timestamp,13)||isNumber(timestamp,10);
    }



    /**
     * 判断是否合法时间戳
     * @param url
     * @return
     */
    public static boolean isAction(String url){

        Pattern pattern = Pattern.compile("^");
        Matcher isNum = pattern.matcher(url);
        if (isNum.matches())return true;
        else return false;
    }
    public static void main(String[] args) {
        System.out.println(isTimeStamp(" \u202D1,562,152,203,772\u202C"));
        System.out.println(isTimeStamp("12121124z2"));
        System.out.println(WmsTimeUtils.timestamToDatetime(Long.valueOf("1212312412131")));
        System.out.println(isTimeStamp(String.valueOf(WmsTimeUtils.datatimeToTimestamp(LocalDateTime.now()))));
    }
}
