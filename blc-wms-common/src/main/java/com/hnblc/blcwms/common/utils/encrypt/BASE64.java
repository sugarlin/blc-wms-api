package com.hnblc.blcwms.common.utils.encrypt;

import org.apache.commons.codec.binary.Base64;

/**
 * BASE64加密解密
 */
public class BASE64 {

	/**
	 * BASE64解密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptBASE64(String key) throws Exception {
		return Base64.decodeBase64(key);
	}

	/**
	 * BASE64加密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String encryptBASE64(byte[] key) throws Exception {
		return Base64.encodeBase64String(key);
	}

	public static void main(String[] args) {
		String sourceStr = "{\n" +
                "        \"custom_no\": \"ZDMZZ\",\n" +
                "        \"enterprise_no\": \"8888\",\n" +
                "        \"operate_details\": {\n" +
                "            \"customs_inspection\": \"0\",\n" +
                "            \"exp_remark\": \"订单来自于通关服务信息交换系统\",\n" +
                "            \"exp_type\": \"2\",\n" +
                "            \"fast_flag\": \"0\",\n" +
                "            \"good_count\": 1,\n" +
                "            \"logistic_no\": \"410198Z062\",\n" +
                "            \"logistic_waybill_no\": \"logisticsNoB0048\",\n" +
                "            \"mail_ie_type\": \"0\",\n" +
                "            \"operate\": \"1\",\n" +
                "            \"order_batch\": \"1565336458888\",\n" +
                "            \"order_details\": [\n" +
                "                {\n" +
                "                    \"article_identifier\": \"4101W681001016040021\",\n" +
                "                    \"barcode\": \"4961989409016\",\n" +
                "                    \"cost\": 200,\n" +
                "                    \"item_type\": \"0\",\n" +
                "                    \"owner_article_no\": \"1016040021\",\n" +
                "                    \"packing_qty\": 1,\n" +
                "                    \"plan_qty\": 1\n" +
                "                }\n" +
                "            ],\n" +
                "            \"order_source\": \"4101W68100\",\n" +
                "            \"receive_address\": \"1\",\n" +
                "            \"receive_city\": \"1\",\n" +
                "            \"receive_province\": \"1\",\n" +
                "            \"receive_zone\": \"1\",\n" +
                "            \"receiver\": \"吕世阳\",\n" +
                "            \"receiver_mobile_no\": \"15890122953\",\n" +
                "            \"send_mobile_phone\": \"1\",\n" +
                "            \"send_telephone\": \"\",\n" +
                "            \"shipper\": \"1\",\n" +
                "            \"shipper_address\": \"1\",\n" +
                "            \"shipper_city\": \"\",\n" +
                "            \"shipper_company_name\": \"\",\n" +
                "            \"shipper_postcode\": \"\",\n" +
                "            \"shipper_province\": \"\",\n" +
                "            \"shipper_street\": \"\",\n" +
                "            \"shipper_zone\": \"\",\n" +
                "            \"sku_count\": 1,\n" +
                "            \"source_exp_no\": \"orderNoB0048\",\n" +
                "            \"take_type\": \"0\"\n" +
                "        },\n" +
                "        \"owner_no\": \"ZDMZZ\",\n" +
                "        \"warehouse_no\": \"001\"\n" +
                "    }";
		String data;
		try {
			data = BASE64.encryptBASE64(sourceStr.getBytes("UTF-8"));
			System.out.println("已加密：" + data);
			byte[] byteArray = BASE64.decryptBASE64(data);
			System.out.println("解密后：" + new String(byteArray));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
