package com.hnblc.blcwms.common.exceptions;

import java.text.DecimalFormat;

/**
 * 
 * ErrorDescription
 *
 * @author <A>cheng.yy@neusoft.com</A>
 *
 * @date Apr 15, 2015
 *
 * @Copyright: © 2001-2015 东软集团股份有限公司
 *
 */
public final class ErrorDescription implements ErrorCode {

    private static final DecimalFormat FORMATTER = new DecimalFormat("00000000");

    // error code
    private final String errorCode;

    /**
     * 创建ErrorDescription
     * 
     * @param errorCode
     * @return ErrorDescription
     */
    private static ErrorDescription create(final int errorCode) {
        return new ErrorDescription(FORMATTER.format(errorCode));
    }

    /**
     * 构造器
     * 
     * @param formatErrorCode
     */
    private ErrorDescription(final String formatErrorCode) {
        this.errorCode = formatErrorCode;
    }

    /**
     * default构造器(屏蔽)
     */
    private ErrorDescription() {
        // Default constructor should not be used.
        this(null);
    }

    /**
     * 获取errorCode
     * 
     * @return errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /********************** SYSTEM ERROR ***************************/

    /**
     * 系统异常。
     */
    public static final ErrorDescription ERROR_OTHER_1 = create(ERROR_OTHER);

    /**
     * 业务异常。
     */
    public static final ErrorDescription ERROR_BUSINESS_1 = create(ERROR_BUSINESS);

    /**
     * 参数个数错误。
     */
    public static final ErrorDescription ERROR_METHOD_PARAMETER_NUMBER_2 = create(ERROR_METHOD_PARAMETER_NUMBER);

    /**
     * 参数值错误。
     */
    public static final ErrorDescription ERROR_METHOD_PARAMETER_VALUE_2 = create(ERROR_METHOD_PARAMETER_VALUE);

    /**
     * 数据库异常.
     */
    public static final ErrorDescription ERROR_SQLEXCEPTION_OCCURRED_1 = create(ERROR_SQLEXCEPTION_OCCURRED);

    /**
     * 网络连接异常.
     */
    public static final ErrorDescription ERROR_HTTPEXCEPTION_OCCURRED_2 = create(ERROR_HTTPEXCEPTION_OCCURRED);

    /**
     * 报文保存路径未配置
     *
     */
    public static final ErrorDescription ERROR_REQUEST_MESSAGE_PATH_UNCONFIG_0 = create(ERROR_REQUEST_MESSAGE_PATH_UNCONFIG);


    /*************************************接口错误信息******************************************/


    /**
     * 请求参数格式错误
     *
     */
    public static final ErrorDescription ERROR_REQUEST_BAD_PARAMETER_FORMAT_0 = create(ERROR_REQUEST_BAD_PARAMETER_FORMAT);
    /**
     * 客户端信息未找到或未启用
     *
     */
    public static final ErrorDescription ERROR_REQUEST_CLIENT_UNAUTHORIZED_0 = create(ERROR_REQUEST_CLIENT_UNAUTHORIZED);
    /**
     * 签名信息验证失败
     *
     */
    public static final ErrorDescription ERROR_REQUEST_SIGNATURE_VALIDATE_FAIL_0 = create(ERROR_REQUEST_SIGNATURE_VALIDATE_FAIL);
    /**
     * 请求内容验证失败
     */
    public static final ErrorDescription ERROR_REQUEST_VALIDATION_FAIL_0 =  create(ERROR_REQUEST_VALIDATION_FAIL);
    /**
     * 请求超时，timpstamp验证失败
     */
    public static final ErrorDescription ERROR_REQUEST_OVER_TIME_0 = create(ERROR_REQUEST_OVER_TIME);
    /**
     * 门店或者货主不存在
     */
    public static final ErrorDescription ERROR_REQUEST_OWNER_NOT_FOUND_0 = create(ERROR_REQUEST_OWNER_NOT_FOUND);
    /**
     *  系统内部错误，联系管理员
     */
    public static final ErrorDescription ERROR_REQUEST_INTERNAL_EXCEPTION_0 = create(ERROR_REQUEST_INTERNAL_EXCEPTION);
    /**
     *  请求内容包含UTF-8不支持编码字符
     */
    public static final ErrorDescription ERROR_REQUEST_NOT_IN_CHARSET_0 = create(ERROR_REQUEST_NOT_IN_CHARSET);
    /**
     *  业务处理未成功,或批量操作部分成功
     */
    public static final ErrorDescription ERROR_REQUEST_BUSINESS_ERROR_0 = create(ERROR_REQUEST_BUSINESS_ERROR);


    /**
     *  未找到该出货单
     */
    public static final ErrorDescription ERROR_WEB_EXPRESS_NOT_FOUND_0 = create(ERROR_WEB_EXPRESS_NOT_FOUND);

    /**
     *  出货单状态不可执行此操作
     */
    public static final ErrorDescription ERROR_WEB_EXPRESS_WRONG_STATUS_0 = create(ERROR_WEB_EXPRESS_WRONG_STATUS);

}
