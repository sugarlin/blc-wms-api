package com.hnblc.blcwms.common.interaction;

import com.hnblc.blcwms.common.constant.CommonConst;
import com.hnblc.blcwms.common.exceptions.GeneralException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Setter
@Getter
@ToString
@EqualsAndHashCode
public class RestResponse<T> {
    private String resultCode;
    private String resultMsg;
    private String errorCode;
    private String errorDetail;
    public static final String RESP_CODE_SUCCESS = "RESP-01";
    public static final String RESP_CODE_FAILURE = "RESP-00";
    private T business_data;
    private RestResponse(){
    }
    public RestResponse(boolean success){
        if (success){
            this.resultCode = RESP_CODE_SUCCESS;
            this.resultMsg = CommonConst.getRespDetail(RESP_CODE_SUCCESS);
        }else {
            this.resultCode = RESP_CODE_FAILURE;
            this.resultMsg = CommonConst.getRespDetail(RESP_CODE_FAILURE);
        }
    }
    public RestResponse(Response<T> commonResponse){
        this.resultCode = commonResponse.getCode();
        this.resultMsg = commonResponse.getLocalizedMessage();
        this.errorCode = commonResponse.getErrorCode();
        this.errorDetail = commonResponse.getFailedMessage();
        this.business_data = commonResponse.getResponseData();
    }
    public RestResponse(GeneralException e){
        this.initError(e);
    }


    public RestResponse initError(GeneralException e){
        this.resultCode = RESP_CODE_FAILURE;
        this.resultMsg = CommonConst.getRespDetail(RESP_CODE_FAILURE);
        this.errorCode = e.getCode();
        this.errorDetail = e.getSingleMessage();
        return this;
    }

    public RestResponse(final String errorCode, final String errorDetail){
        this.resultCode = RESP_CODE_FAILURE;
        this.resultMsg = CommonConst.getRespDetail(RESP_CODE_FAILURE);
        this.errorCode = errorCode;
        this.errorDetail = errorDetail;
    }

}
