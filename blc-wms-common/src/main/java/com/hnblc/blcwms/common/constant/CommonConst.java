package com.hnblc.blcwms.common.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class CommonConst {

    public static final ResourceBundle responseDetail;
    static{
        responseDetail = ResourceBundle.getBundle("i18n/messages", Locale.getDefault());
    }



    /**
     * 缓存KEY
     */
    public static final String CACHE_KEY_SYSTEM_DICTIONARIES_LIST = "sys_dictionaries_list";               //系统字典表缓存KEY
    public static final String CACHE_KEY_SYSTEM_DICTIONARIES_MAP = "sys_dictionaries_map";               //系统字典表缓存KEY


    public static final String CACHE_KEY_BUSINESS_DEFINITION_OWNER = "business.definition.owner";               //业务基本定义缓存KEY
    public static final String CACHE_KEY_BUSINESS_DEFINITION_SHIPPER = "business.definition.shipper";               //业务基本定义缓存KEY
    public static final String CACHE_KEY_BUSINESS_DEFINITION_ARTICLE = "business.definition.article";               //业务基本定义缓存KEY
    public static final String CACHE_KEY_BUSINESS_DEFINITION_CUSTOM = "business.definition.cust";               //业务基本定义缓存KEY
    public static final String CACHE_KEY_BUSINESS_DEFINITION_SUPPLIER = "business.definition.supplier";               //业务基本定义缓存KEY




    public static final String getRespDetail(String respKey){
        return CommonConst.responseDetail.getString(respKey);
    }

    /**
     * custom static constant
     */
    public static String SYS_VERSION;
    public static String SYS_INTERFACE_DATA_LOG_PATH_WIN;
    public static String SYS_INTERFACE_DATA_LOG_PATH_UNIX;

    @Value("${sys.version}")
    public void setSysVersion(final String sysVersion) {
        SYS_VERSION = sysVersion;
    }
    @Value("${sys.interface.data.log.path.win}")
    public void setSysInterfaceDataLogPathWin(final String sysInterfaceDataLogPathWin) {
        SYS_INTERFACE_DATA_LOG_PATH_WIN = sysInterfaceDataLogPathWin;
    }

    @Value("${sys.interface.data.log.path.unix}")
    public void setSysInterfaceDataLogPathUnix(final String sysInterfaceDataLogPathUnix) {
        SYS_INTERFACE_DATA_LOG_PATH_UNIX = sysInterfaceDataLogPathUnix;
    }
}
