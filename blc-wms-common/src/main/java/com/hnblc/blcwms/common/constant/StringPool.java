package com.hnblc.blcwms.common.constant;

public class StringPool {
    /**
     * charset name string constant
     * 字符集名称常量
     */
    public static final String CHARSET_UTF8 = "UTF-8";
    public static final String CHARSET_UTF16 = "UTF-16";
    public static final String CHARSET_GBK = "GBK";
    public static final String CHARSET_ISO88591 = "ISO-8859-1";
    public static final String CHARSET_ASCII = "ASCII";
    public static final String CHARSET_GB2312 = "GB2312";

    /**
     * file suffix string constant
     * 文件后缀名常量
     */
    public static final String FILE_SUFFIX_DOT_XML = ".xml";
    public static final String FILE_SUFFIX_DOT_TXT = ".txt";



    /**
     * file suffix string constant
     * HTTP METHOD 常量
     */
    public static final String HTTP_METHOD_POST = "POST";
    public static final String HTTP_METHOD_GET = "GET";
    public static final String HTTP_METHOD_DELETE = "DELETE";
    public static final String HTTP_METHOD_PUT = "PUT";


    /**
     * 常用分隔符
     */

    public static final String COMMON_SEPARATOR_SPLIT_COLON= ":";
    public static final String COMMON_SEPARATOR_SPLIT_ASTERISK = "*";
    public static final String COMMON_SEPARATOR_SPLIT_VERTICAL_LINE = "|";
    public static final String COMMON_SEPARATOR_SPLIT_PERIOD = ".";
    public static final String COMMON_SEPARATOR_SPLIT_COMMA = ",";


    /**
     * 转义字符
     */
    public static final String TRANSFERRED_MEANING_LINE_BREAK= "\n";
    public static final String TRANSFERRED_MEANING_LINE_NEW= "\t";
    /**
     *  MESSAGE EXPRESSION {key}
     *  配置文件可变文本参数名称{key}
     */
    public static final String MESSAGE_EXCEPTION_KEY_FIELD = "field";
}
