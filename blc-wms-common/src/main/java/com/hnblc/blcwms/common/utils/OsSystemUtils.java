package com.hnblc.blcwms.common.utils;

import com.hnblc.blcwms.common.constant.CommonConst;
import com.hnblc.blcwms.common.exceptions.GlobalCustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * Created by linsong on 2018/9/14.
 */
public class OsSystemUtils {

    private static final Logger logger = LoggerFactory.getLogger(OsSystemUtils.class);
    public static String getOsInterfacePath() throws GlobalCustomException{
        String os = System.getProperty("os.name");
        if(os.toLowerCase().startsWith("win")){
            if (StringUtils.isEmpty(CommonConst.SYS_INTERFACE_DATA_LOG_PATH_WIN)){
                return System.getProperty("user.dir");
            }else{
                return CommonConst.SYS_INTERFACE_DATA_LOG_PATH_WIN;
            }
        }else{
            if (StringUtils.isEmpty(CommonConst.SYS_INTERFACE_DATA_LOG_PATH_UNIX)){
                return System.getProperty("user.dir");
            }else{
                return CommonConst.SYS_INTERFACE_DATA_LOG_PATH_UNIX;
            }
        }
    }

    public static String getOsLineBreak(){
        if(System.getProperty("os.name").toLowerCase().startsWith("win")){
            return "\r\n\r\n";
        }else{
            return "\n\n";
        }
    }


}
