package com.hnblc.blcwms.common.exceptions;

public class ManualValidationFailException extends RuntimeException {


    private static final long serialVersionUID = 1376737272399328681L;

    public ManualValidationFailException(){
        super();
    }

    public ManualValidationFailException(String message){
        super(message);
    }

    public ManualValidationFailException(String message, Throwable cause){
        super(message, cause);
    }

    public ManualValidationFailException(Throwable cause){
        super(cause);
    }
}
