package com.hnblc.blcwms.common.exceptions;

public class GlobalCustomException extends GeneralException {

    private static final long serialVersionUID = 6583355178020429643L;

    public  GlobalCustomException(final ErrorDescription errorDescriptio){
        super(errorDescriptio);
    }

    public GlobalCustomException(final ErrorDescription errorDescription,
                                 final Object... args){
        super(errorDescription,args);
    }

    public GlobalCustomException(final int option, final Object... args){
        super(option,args);
    }

    public GlobalCustomException(Throwable cause){
        super(cause);
    }
}
