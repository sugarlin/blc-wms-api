package com.hnblc.blcwms.common.exceptions;

public class ClientInfoNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 8572727554550169944L;

    public ClientInfoNotFoundException(){
        super();
    }

    public ClientInfoNotFoundException(String message){
        super(message);
    }

    public ClientInfoNotFoundException(String message, Throwable cause){
        super(message, cause);
    }

    public ClientInfoNotFoundException(Throwable cause){
        super(cause);
    }
}
