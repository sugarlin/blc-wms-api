package com.hnblc.blcwms.common.exceptions;

/**
 * 
 * ErrorCode
 *
 * @author <A>cheng.yy@neusoft.com</A>
 *
 * @date Apr 15, 2015
 *
 * @Copyright: © 2001-2015 东软集团股份有限公司
 *
 */
public interface ErrorCode {

    /********************** SYSTEM ERROR ***************************/
    /**
     * 系统异常。
     */
    int ERROR_OTHER = 10000000;

    /**
     * 业务异常。
     */
    int ERROR_BUSINESS = 10000001;

    /**
     * 参数个数错误。
     */
    int ERROR_METHOD_PARAMETER_NUMBER = 10000002;

    /**
     * 参数值错误。
     */
    int ERROR_METHOD_PARAMETER_VALUE = 10000003;

    /**
     * 数据库异常.
     */
    int ERROR_SQLEXCEPTION_OCCURRED = 10000004;

    /**
     * 网络连接异常.
     */
    int ERROR_HTTPEXCEPTION_OCCURRED = 10000005;
    /**
     * 报文保存路径未配置
     */
    int ERROR_REQUEST_MESSAGE_PATH_UNCONFIG = 10000006;

    /**********************************************************接口请求异常********************************************************/

    /**
     * 请求参数格式错误，请检查。
     */
    int ERROR_REQUEST_BAD_PARAMETER_FORMAT = 11000001;
    /**
     * 该客户端未授权或不存在
     */
    int ERROR_REQUEST_CLIENT_UNAUTHORIZED = 11000002;
    /**
     * 签名信息验证失败
     */
    int ERROR_REQUEST_SIGNATURE_VALIDATE_FAIL= 11000003;
    /**
     * 请求报文验证失败
     */
    int ERROR_REQUEST_VALIDATION_FAIL = 11000004;
    /**
     * 请求超时
     */
    int ERROR_REQUEST_OVER_TIME = 11000005;
    /**
     * 货主或门店信息不存在
     */
    int ERROR_REQUEST_OWNER_NOT_FOUND = 11000006;
    /**
     * 系统内部错误异常，联系管理员
     */
    int ERROR_REQUEST_INTERNAL_EXCEPTION= 11000007;
    /**
     * 请求内容包含UTF-8不支持编码字符
     */
    int ERROR_REQUEST_NOT_IN_CHARSET = 11000008;
    /**
     * 业务处理未成功,或批量操作部分成功
     */
    int ERROR_REQUEST_BUSINESS_ERROR = 11000009;
    /**
     * 出货单未找到
     */
    int ERROR_WEB_EXPRESS_NOT_FOUND = 12000001;
    /**
     * 不符合业务类型的出货状态
     */
    int ERROR_WEB_EXPRESS_WRONG_STATUS = 12000002;
}
