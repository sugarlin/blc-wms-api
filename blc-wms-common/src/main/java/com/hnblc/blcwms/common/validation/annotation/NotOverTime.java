package com.hnblc.blcwms.common.validation.annotation;

import com.hnblc.blcwms.common.validation.validator.TimeOutValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Constraint(validatedBy = {TimeOutValidator.class})
public @interface NotOverTime {

    long maxOverTime() default 300000;
    String message() default "request timeStamp over time";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
