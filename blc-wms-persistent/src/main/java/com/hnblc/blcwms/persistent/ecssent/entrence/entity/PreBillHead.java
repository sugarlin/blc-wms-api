package com.hnblc.blcwms.persistent.ecssent.entrence.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class PreBillHead  implements Serializable {
    private static final long serialVersionUID = -4996067710376716117L;

    /**
     * 清单编号 ，报关申请单号
     */
    private String billNo;
    /**
     * 报关单号
     */
    private String entryId;
    /**
     * 进出口岸
     */
    private String InEPort;

    /**
     * 航班号
     */
    private String voyageNo;
    /**
     * 申报时间
     */
    private Date declDate;
    /**
     * 第一审批时间
     */
    private Date apprDate1;

}
