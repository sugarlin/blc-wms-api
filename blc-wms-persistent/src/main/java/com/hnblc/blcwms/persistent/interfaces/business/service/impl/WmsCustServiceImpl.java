package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsCust;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.WmsCustMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsCustService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-21
 */
@Service
public class WmsCustServiceImpl extends ServiceImpl<WmsCustMapper, WmsCust> implements IWmsCustService {

}
