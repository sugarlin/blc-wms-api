package com.hnblc.blcwms.persistent.interfaces.business.mapper;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsRet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-12-17
 */
public interface WmsRetMapper extends BaseMapper<WmsRet> {

}
