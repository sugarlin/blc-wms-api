package com.hnblc.blcwms.persistent.business.odata.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-03-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ODATA_EXP_D")
public class OdataExpD implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 仓别
     */
    @TableField("WAREHOUSE_NO")
    private String warehouseNo;

    /**
     * 货主
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 出货单号
     */
    @TableField("EXP_NO")
    private String expNo;

    /**
     * 商品编码
     */
    @TableField("ARTICLE_NO")
    private String articleNo;

    /**
     * 包装数量
     */
    @TableField("PACKING_QTY")
    private Double packingQty;

    /**
     * 计划数量
     */
    @TableField("ARTICLE_QTY")
    private Double articleQty;

    /**
     * 调度数量
     */
    @TableField("SCHEDULE_QTY")
    private Double scheduleQty;

    /**
     * 定位数量
     */
    @TableField("LOCATE_QTY")
    private Double locateQty;

    /**
     * 实际出货数量
     */
    @TableField("REAL_QTY")
    private Double realQty;

    /**
     * 单价
     */
    @TableField("UNIT_COST")
    private Double unitCost;

    /**
     * 货主商品编码
     */
    @TableField("OWNER_ARTICLE_NO")
    private String ownerArticleNo;

    /**
     * 生产日期条件
     */
    @TableField("PRODUCE_CONDITION")
    private String produceCondition;

    /**
     * 生产日期值1
     */
    @TableField("PRODUCE_VALUE1")
    private LocalDateTime produceValue1;

    /**
     * 生产日期值2
     */
    @TableField("PRODUCE_VALUE2")
    private LocalDateTime produceValue2;

    /**
     * 到期日至条件:1->=；2-<=；3>；4-<；5-=；6-like；7-between；A-单一生产日期；B-促销生产日期；C：单一促销生产日期；D：清理生产日期；E：单一清理生产日期；F：新生产日期； G：单一新生产日期；
     */
    @TableField("EXPIRE_CONDITION")
    private String expireCondition;

    /**
     * 到期日至值1
     */
    @TableField("EXPIRE_VALUE1")
    private LocalDateTime expireValue1;

    /**
     * 到期日至值2
     */
    @TableField("EXPIRE_VALUE2")
    private LocalDateTime expireValue2;

    /**
     * 品质条件
     */
    @TableField("QUALITY_CONDITION")
    private String qualityCondition;

    /**
     * 品质值1
     */
    @TableField("QUALITY_VALUE1")
    private String qualityValue1;

    /**
     * 品质值2
     */
    @TableField("QUALITY_VALUE2")
    private String qualityValue2;

    /**
     * 批号条件
     */
    @TableField("LOTNO_CONDITION")
    private String lotnoCondition;

    /**
     * 批号值1
     */
    @TableField("LOTNO_VALUE1")
    private String lotnoValue1;

    /**
     * 批号值2
     */
    @TableField("LOTNO_VALUE2")
    private String lotnoValue2;

    /**
     * 预留批属性1条件
     */
    @TableField("RSVBATCH1_CONDITION")
    private String rsvbatch1Condition;

    /**
     * 预留批属性1值1
     */
    @TableField("RSVBATCH1_VALUE1")
    private String rsvbatch1Value1;

    /**
     * 预留批属性1值2
     */
    @TableField("RSVBATCH1_VALUE2")
    private String rsvbatch1Value2;

    /**
     * 预留批属性2条件
     */
    @TableField("RSVBATCH2_CONDITION")
    private String rsvbatch2Condition;

    /**
     * 预留批属性2值1
     */
    @TableField("RSVBATCH2_VALUE1")
    private String rsvbatch2Value1;

    /**
     * 预留批属性2值2
     */
    @TableField("RSVBATCH2_VALUE2")
    private String rsvbatch2Value2;

    /**
     * 预留批属性3条件
     */
    @TableField("RSVBATCH3_CONDITION")
    private String rsvbatch3Condition;

    /**
     * 预留批属性3值1
     */
    @TableField("RSVBATCH3_VALUE1")
    private String rsvbatch3Value1;

    /**
     * 预留批属性3值2
     */
    @TableField("RSVBATCH3_VALUE2")
    private String rsvbatch3Value2;

    /**
     * 预留批属性4条件
     */
    @TableField("RSVBATCH4_CONDITION")
    private String rsvbatch4Condition;

    /**
     * 预留批属性4值1
     */
    @TableField("RSVBATCH4_VALUE1")
    private String rsvbatch4Value1;

    /**
     * 预留批属性4值2
     */
    @TableField("RSVBATCH4_VALUE2")
    private String rsvbatch4Value2;

    /**
     * 预留批属性5条件
     */
    @TableField("RSVBATCH5_CONDITION")
    private String rsvbatch5Condition;

    /**
     * 预留批属性5值1
     */
    @TableField("RSVBATCH5_VALUE1")
    private String rsvbatch5Value1;

    /**
     * 预留批属性5值2
     */
    @TableField("RSVBATCH5_VALUE2")
    private String rsvbatch5Value2;

    /**
     * 预留批属性6条件
     */
    @TableField("RSVBATCH6_CONDITION")
    private String rsvbatch6Condition;

    /**
     * 预留批属性6值1
     */
    @TableField("RSVBATCH6_VALUE1")
    private String rsvbatch6Value1;

    /**
     * 预留批属性6值2
     */
    @TableField("RSVBATCH6_VALUE2")
    private String rsvbatch6Value2;

    /**
     * 预留批属性7条件
     */
    @TableField("RSVBATCH7_CONDITION")
    private String rsvbatch7Condition;

    /**
     * 预留批属性7值1
     */
    @TableField("RSVBATCH7_VALUE1")
    private String rsvbatch7Value1;

    /**
     * 预留批属性7值2
     */
    @TableField("RSVBATCH7_VALUE2")
    private String rsvbatch7Value2;

    /**
     * 预留批属性8条件
     */
    @TableField("RSVBATCH8_CONDITION")
    private String rsvbatch8Condition;

    /**
     * 预留批属性8值1
     */
    @TableField("RSVBATCH8_VALUE1")
    private String rsvbatch8Value1;

    /**
     * 预留批属性8值2
     */
    @TableField("RSVBATCH8_VALUE2")
    private String rsvbatch8Value2;

    /**
     * 指定字段名
     */
    @TableField("SPECIFY_FIELD")
    private String specifyField;

    /**
     * 指定条件
     */
    @TableField("SPECIFY_CONDITION")
    private String specifyCondition;

    /**
     * 指定值1
     */
    @TableField("SPECIFY_VALUE1")
    private String specifyValue1;

    /**
     * 指定值2
     */
    @TableField("SPECIFY_VALUE2")
    private String specifyValue2;

    /**
     * 状态
     */
    @TableField("STATUS")
    private String status;

    /**
     * 错误状态：调度时会对接口转入的数据进行校验，当发现问题数据，给予相应的错误状态。
     */
    @TableField("ERROR_STATUS")
    private String errorStatus;

    /**
     * 建立日期
     */
    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;

    /**
     * 定位日期
     */
    @TableField("EXP_DATE")
    private LocalDateTime expDate;

    /**
     * 企业编号
     */
    @TableId("ENTERPRISE_NO")
    private String enterpriseNo;

    @TableField("ROW_ID")
    private Integer rowId;

    /**
     * 订单商品类型：0-普通；1-赠品(免拣货)
     */
    @TableField("ITEM_TYPE")
    private String itemType;


}
