package com.hnblc.blcwms.persistent.business.odata.vo;

import lombok.Data;

import java.util.List;

@Data
public class ExpressInfoHead {

    private String enterpriseNo;
    private String warehouseNo;
    private String expNo;
    private String sourceexpNo;
    private String waybillNo;
    private String orderSource;
    private String expType;
    private String orderNo;
    private String senderName;
    private String senderProvince;
    private String senderCity;
    private String senderZone;
    private String senderAddress;
    private String senderMobile;
    private String senderPhone;
    private String senderPostNo;
    private String receiveName;
    private String receiveProvince;
    private String receiveCity;
    private String receiveZone;
    private String receiveAddress;
    private String receiveMobile;
    private String receivePhone;
    private String receivePostNo;
    private String remark;



    private List<ExpressInfoBody> goodsList;
}
