package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.ConvertOrgSet;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.ConvertOrgSetMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IConvertOrgSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-26
 */
@Service
public class ConvertOrgSetServiceImpl extends ServiceImpl<ConvertOrgSetMapper, ConvertOrgSet> implements IConvertOrgSetService {

}
