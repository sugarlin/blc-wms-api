package com.hnblc.blcwms.persistent.interfaces.client.service.impl;

import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.persistent.interfaces.client.mapper.ClientAuthMapper;
import com.hnblc.blcwms.persistent.interfaces.client.service.IClientAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用于查询客户端有哪些客户的查询权限 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
@Service
public class ClientAuthServiceImpl extends ServiceImpl<ClientAuthMapper, ClientAuth> implements IClientAuthService {

}
