package com.hnblc.blcwms.persistent.business.basedefine.service.impl;

import com.hnblc.blcwms.persistent.business.basedefine.entity.Owner;
import com.hnblc.blcwms.persistent.business.basedefine.mapper.OwnerMapper;
import com.hnblc.blcwms.persistent.business.basedefine.service.IOwnerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Service
public class OwnerServiceImpl extends ServiceImpl<OwnerMapper, Owner> implements IOwnerService {

}
