package com.hnblc.blcwms.persistent.business.odata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpM;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressInfoHead;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;
import com.hnblc.blcwms.persistent.business.odata.vo.TraceInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-03-08
 */
public interface OdataExpMMapper extends BaseMapper<OdataExpM> {



    void cancelExpress(OdataExpM odataExpM);

    /**
     * 查询包裹对应商品溯源信息
     * @param expNo
     * @return
     */
    List<TraceInfo> selectTraceInfo(String expNo);

    /**
     * 订单主要信息常用查询
     * @param queryVO
     * @return
     */
    List<ExpressInfoHead> getExpressFullInfo(@Param("exp") ExpressQueryVO queryVO);


}
