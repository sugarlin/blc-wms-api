package com.hnblc.blcwms.persistent.ecssent.entrence.entity;

import lombok.Data;

@Data
public class GoodImportDetail {
    private String customsIdentifier;
    private String importDateTime;
    private String importDeclareNo;
    private String goodName;
}
