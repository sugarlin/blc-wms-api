package com.hnblc.blcwms.persistent.interfaces.sysConfig.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnblc.blcwms.persistent.interfaces.sysConfig.entity.FieldDictionary;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 接口系统字段字典表 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-02-13
 */
public interface IFieldDictionaryService extends IService<FieldDictionary> {
    List<FieldDictionary> listLoadInCache(String tableName, String column);
    Map<String,FieldDictionary> mapLoadInCache(String tableName, String column);
}
