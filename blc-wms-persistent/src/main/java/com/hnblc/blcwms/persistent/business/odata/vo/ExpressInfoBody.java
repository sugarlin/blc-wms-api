package com.hnblc.blcwms.persistent.business.odata.vo;

import lombok.Data;

@Data
public class ExpressInfoBody {

    private String articleNo;
    private String articleIdentifier;
    private String ownerArticleNo;
    private String goodName;
    private Double unitWeight=0.0;
    private Double unitCost=0.0;
    private Double planQty;
    private Double packingQty;
    private String barcode;
    private String rowId;

}
