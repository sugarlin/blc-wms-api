package com.hnblc.blcwms.persistent.interfaces.business.dto;

import com.hnblc.blcwms.persistent.interfaces.business.entity.ErpExpStatus;
import lombok.Data;

/**
 * @ClassName: StatusReadyTask
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/11/2
 * @Version: V1.0
 */
@Data
public class StatusReadyTask extends ErpExpStatus {
    private static final long serialVersionUID = 3650600019795565486L;
    private String clientKey;
    private String clientReturnUrl;
}
