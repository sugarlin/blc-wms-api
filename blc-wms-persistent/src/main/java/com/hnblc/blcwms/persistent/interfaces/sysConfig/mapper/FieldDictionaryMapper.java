package com.hnblc.blcwms.persistent.interfaces.sysConfig.mapper;

import com.hnblc.blcwms.persistent.interfaces.sysConfig.entity.FieldDictionary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 接口系统字段字典表 mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-02-13
 */
public interface FieldDictionaryMapper extends BaseMapper<FieldDictionary> {

}
