package com.hnblc.blcwms.persistent.interfaces.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnblc.blcwms.persistent.interfaces.client.dto.CurrentClient;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientInfo;

/**
 * <p>
 * 接口客户端信息表 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
public interface IClientInfoService extends IService<ClientInfo> {
    CurrentClient loadCurrentClient(ClientAuth clientAuth);


}
