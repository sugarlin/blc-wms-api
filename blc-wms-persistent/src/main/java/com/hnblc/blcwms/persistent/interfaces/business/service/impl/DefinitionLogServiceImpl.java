package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.DefinitionLog;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.DefinitionLogMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IDefinitionLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 接口操作基础资料记录 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-28
 */
@Service
public class DefinitionLogServiceImpl extends ServiceImpl<DefinitionLogMapper, DefinitionLog> implements IDefinitionLogService {

}
