package com.hnblc.blcwms.persistent.business.basedefine.service.impl;

import com.hnblc.blcwms.persistent.business.basedefine.entity.Loc;
import com.hnblc.blcwms.persistent.business.basedefine.mapper.LocMapper;
import com.hnblc.blcwms.persistent.business.basedefine.service.ILocService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Service
public class LocServiceImpl extends ServiceImpl<LocMapper, Loc> implements ILocService {

}
