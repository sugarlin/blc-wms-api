package com.hnblc.blcwms.persistent.interfaces.custom.service;

import com.hnblc.blcwms.persistent.interfaces.custom.entity.CustomLogisInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 保存客户请求承运商信息 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-05-27
 */
public interface ICustomLogisInfoService extends IService<CustomLogisInfo> {

}
