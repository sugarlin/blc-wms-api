package com.hnblc.blcwms.persistent.interfaces.logistics.mapper;

import com.hnblc.blcwms.persistent.interfaces.logistics.entity.SenderAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 网点下可用的发货地址，可根据网点和企业号仓别号获取发货地址 Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-12-05
 */
public interface SenderAddressMapper extends BaseMapper<SenderAddress> {


    @Select("select LSA.BRANCH_CODE,\n" +
            "LSA.ENTERPRISE_NO,\n" +
            "LSA.WAREHOUSE_NO,\n" +
            "LSA.PROVINCE,\n" +
            "LSA.CITY,\n" +
            "LSA.DISTRICT,\n" +
            "LSA.DETAIL from i_l_sender_address LSA,i_l_logistic_branch_info LLB\n" +
            "WHERE LSA.BRANCH_CODE = LLB.BRANCH_CODE\n" +
            "AND LLB.LOGISTIC_CODE = #{logisticCode,jdbcType=VARCHAR}\n" +
            "AND LSA.ENTERPRISE_NO = #{enterpriseNo,jdbcType=VARCHAR}\n" +
            "AND LSA.WAREHOUSE_NO = #{warehouseNo,jdbcType=VARCHAR}")
    SenderAddress getOneByLogisticCode(String logisticCode,String enterpriseNo,String warehouseNo);
}
