package com.hnblc.blcwms.persistent.interfaces.custom.mapper;

import com.hnblc.blcwms.persistent.interfaces.custom.entity.CustomInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户在传单过程中是否有自定义收件人等信息 Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-05-27
 */
public interface CustomInfoMapper extends BaseMapper<CustomInfo> {

}
