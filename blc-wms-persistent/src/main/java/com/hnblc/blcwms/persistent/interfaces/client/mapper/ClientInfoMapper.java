package com.hnblc.blcwms.persistent.interfaces.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnblc.blcwms.persistent.interfaces.client.dto.CurrentClient;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientInfo;

/**
 * <p>
 * 接口客户端信息表 Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
public interface ClientInfoMapper extends BaseMapper<ClientInfo> {

    CurrentClient loadCurrentClient(ClientAuth clientAuth);
}
