package com.hnblc.blcwms.persistent.interfaces.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnblc.blcwms.persistent.interfaces.message.entity.MessageLog;

/**
 * <p>
 * 记录接口接收内容情况 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
public interface IMessageLogService extends IService<MessageLog> {
}
