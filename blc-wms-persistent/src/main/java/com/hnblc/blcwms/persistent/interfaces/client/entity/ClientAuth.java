package com.hnblc.blcwms.persistent.interfaces.client.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用于查询客户端有哪些客户的查询权限
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("I_C_CLIENT_AUTH")
public class ClientAuth implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 客户端ID
     */
    @TableField("CLIENT_ID")
    private String clientId;

    /**
     * 企业代码
     */
    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 仓别代码
     */
    @TableField("WAREHOUSE_NO")
    private String warehouseNo;

    /**
     * 货主代码
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 销售客户代码
     */
    @TableField("CUSTOM_NO")
    private String customNo;


    @Override
    public boolean equals(final Object o) {
        if(o == this) {
            return true;
        }else if(!(o instanceof ClientAuth)) {
            return false;
        }else{
            ClientAuth other = (ClientAuth) o;
            if (
                    this.getClientId().equalsIgnoreCase(other.getClientId())
                            && this.getEnterpriseNo().equalsIgnoreCase(other.getEnterpriseNo())
                            && this.getWarehouseNo().equalsIgnoreCase(other.getWarehouseNo())
                            && this.getOwnerNo().equalsIgnoreCase(other.getOwnerNo())
                            && this.getCustomNo().equalsIgnoreCase(other.getCustomNo())
            ) return true;
        }
        return false;
    }
}
