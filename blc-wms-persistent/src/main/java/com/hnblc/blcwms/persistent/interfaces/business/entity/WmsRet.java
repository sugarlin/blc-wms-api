package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_RET")
public class WmsRet implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 单据编号（退货订单）
     */
    @TableId("SHEETID")
    private String sheetid;

    /**
     * 商品编码
     */
    @TableField("GOODSID")
    private String goodsid;

    /**
     * 退厂数量
     */
    @TableField("REALQTY")
    private Double realqty;

    /**
     * 商品的包装系数
     */
    @TableField("PKCOUNT")
    private Double pkcount;

    /**
     * 供应商编码
     */
    @TableField("VENDERID")
    private String venderid;

    /**
     * 审核日期
     */
    @TableField("SDATE")
    private LocalDateTime sdate;

    /**
     * 操作员
     */
    @TableField("OPERATOR")
    private String operator;

    /**
     * 退货类型：1：清场退厂；2：总部仓退厂；3：质量仓退厂。（普通退货）
     */
    @TableField("PO_TYPE")
    private String poType;

    /**
     * 仓库编码
     */
    @TableField("PALLETZONE")
    private String palletzone;

    /**
     * ERP的机构代码
     */
    @TableField("SHOP_NO")
    private String shopNo;

    /**
     * 委托业主编码
     */
    @TableField("CUSTOMID")
    private String customid;

    @TableField("ORDERUNIT")
    private String orderunit;

    @TableField("ORDERQUANTITY")
    private Double orderquantity;

    @TableField("UNITRATE")
    private Double unitrate;


}
