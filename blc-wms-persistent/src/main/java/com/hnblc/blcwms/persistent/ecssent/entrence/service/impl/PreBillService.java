package com.hnblc.blcwms.persistent.ecssent.entrence.service.impl;

import com.hnblc.blcwms.persistent.ecssent.entrence.entity.PreBillHead;
import com.hnblc.blcwms.persistent.ecssent.entrence.mapper.PreBillMapper;
import com.hnblc.blcwms.persistent.ecssent.entrence.service.IPreBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PreBillService implements IPreBillService {


    @Autowired
    PreBillMapper preBillMapper;

    @Override
    public List<PreBillHead> getGoodImportDeclinfo(List<String> billNos) {
        return preBillMapper.getGoodImportDeclinfos(billNos);
    }

    @Override
    public PreBillHead getGoodImportDeclinfo(String billNo) {
        return preBillMapper.getGoodImportDeclinfo(billNo);
    }


}
