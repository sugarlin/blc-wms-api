package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_CUST")
public class WmsCust implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 1、传单单据编号2、传单通讯使用(建议使用自动增加的系列号)
     */
    @TableId("SHEETID")
    private String sheetid;

    /**
     * 1、客户编码2、门店编码
     */
    @TableField("OWNER_CUST_NO")
    private String ownerCustNo;

    /**
     * 1、客户名称2、门店名称
     */
    @TableField("CUST_NAME")
    private String custName;

    /**
     * 门店类型：0：自营,1：加盟,2：仓库，3：批发客户
     */
    @TableField("CUST_TYPE")
    private Integer custType;

    /**
     * 门店负责人名称
     */
    @TableField("MANAGER")
    private String manager;

    /**
     * 门店联络人名称
     */
    @TableField("LINKMAN")
    private String linkman;

    /**
     * 门店联络地址
     */
    @TableField("ADDRESS")
    private String address;

    /**
     * 门店邮编
     */
    @TableField("ZIPCODE")
    private String zipcode;

    /**
     * 联络电话
     */
    @TableField("TELE")
    private String tele;

    /**
     * 传真号码
     */
    @TableField("FAX")
    private String fax;

    /**
     * 联络人E-Mail
     */
    @TableField("E_MAIL")
    private String eMail;

    /**
     * 说明与备注
     */
    @TableField("NOTES")
    private String notes;

    /**
     * 状态:1:正常;0(不正常). 
     */
    @TableField("STATUS")
    private Integer status;

    /**
     * 运营部门信息 
     */
    @TableField("DEPARTMENT_NO")
    private String departmentNo;

    /**
     * 省份
     */
    @TableField("PROVINCE")
    private String province;

    /**
     * 市
     */
    @TableField("CITY")
    private String city;

    /**
     * 县
     */
    @TableField("ZONE")
    private String zone;

    /**
     * 企业编码：无需插入值，仅保留默认值即可
     */
    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 委托业主编码：无需插入值，仅保留默认值即可
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 接收时间
     */
    @TableField("HANDLETIME")
    private LocalDateTime handletime;

    /**
     * 客户简称
     */
    @TableField("CUST_ALIAS")
    private String custAlias;


}
