package com.hnblc.blcwms.persistent.interfaces.custom.mapper;

import com.hnblc.blcwms.persistent.interfaces.custom.entity.CustomLogisInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 保存客户请求承运商信息 Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-05-27
 */
public interface CustomLogisInfoMapper extends BaseMapper<CustomLogisInfo> {

}
