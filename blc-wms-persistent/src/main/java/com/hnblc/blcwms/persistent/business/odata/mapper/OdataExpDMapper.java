package com.hnblc.blcwms.persistent.business.odata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpD;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpDeliveryInfoVO;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-03-08
 */
public interface OdataExpDMapper extends BaseMapper<OdataExpD> {


    List<ExpDeliveryInfoVO> getDeliveryInfo(@Param("exp") ExpressQueryVO expDetailVO);

}
