package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_RETRATION")
public class WmsRetration implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ERP门店退货通知单号
     */
    @TableId("SHEETID")
    private String sheetid;

    /**
     * 退货商品编码
     */
    @TableField("GOODSID")
    private String goodsid;

    /**
     * 返配类型
     */
    @TableField("RTYPE")
    private Double rtype;

    /**
     * 委托业主编码
     */
    @TableField("CUSTOMID")
    private String customid;

    /**
     * 仓库编码
     */
    @TableField("PALLETZONE")
    private String palletzone;

    /**
     * 门店编码
     */
    @TableField("SHOPID")
    private String shopid;

    /**
     * 预定退货日期
     */
    @TableField("SDATE")
    private LocalDateTime sdate;

    /**
     * 单内序号
     */
    @TableField("SERIALID")
    private Double serialid;

    /**
     * 包装数量
     */
    @TableField("PKCOUNT")
    private Double pkcount;

    /**
     * 预定退货数量
     */
    @TableField("PLANQTY")
    private Double planqty;

    /**
     * 供应商编码：退供应商时,要加入供应商编码
     */
    @TableField("VENDERID")
    private String venderid;

    /**
     * 说明
     */
    @TableField("NOTES")
    private String notes;

    /**
     * ERP的机构代码
     */
    @TableField("SHOP_NO")
    private String shopNo;

    @TableField("ORDERUNIT")
    private String orderunit;

    @TableField("ORDERQUANTITY")
    private Double orderquantity;

    @TableField("UNITRATE")
    private Double unitrate;

    @TableField("ERP_RETURN_NO")
    private String erpReturnNo;

    /**
     * 品质类型：1良品，A不良品
     */
    @TableField("QUALITY_TYPE")
    private String qualityType;


}
