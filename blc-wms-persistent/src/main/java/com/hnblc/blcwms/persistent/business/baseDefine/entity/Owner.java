package com.hnblc.blcwms.persistent.business.basedefine.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("BDEF_DEFOWNER")
public class Owner implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 委托业主编码
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 委托业主名称
     */
    @TableField("OWNER_NAME")
    private String ownerName;

    /**
     * 委托业主简称
     */
    @TableField("OWNER_ALIAS")
    private String ownerAlias;

    /**
     * 委托业主地址
     */
    @TableField("OWNER_ADDRESS")
    private String ownerAddress;

    /**
     * 委托业主电话
     */
    @TableField("OWNER_PHONE")
    private String ownerPhone;

    /**
     * 委托业主传真
     */
    @TableField("OWNER_FAX")
    private String ownerFax;

    /**
     * 委托业主联系人
     */
    @TableField("OWNER_CONTACT")
    private String ownerContact;

    /**
     * 委托业主备注
     */
    @TableField("OWNER_REMARK")
    private String ownerRemark;

    /**
     * 发票号
     */
    @TableField("INVOICE_NO")
    private String invoiceNo;

    /**
     * 发票地址
     */
    @TableField("INVOICE_ADDR")
    private String invoiceAddr;

    /**
     * 发票抬头
     */
    @TableField("INVOICE_HEADER")
    private String invoiceHeader;

    /**
     * 状态  1-正常；0-停用；

     */
    @TableField("STATUS")
    private String status;

    /**
     * 商品周转规则：.FIFO,FEFO,LIFO

     */
    @TableField("TURN_OVER_RULE")
    private String turnOverRule;

    /**
     * 货主是否绑定固定储位，0：不绑定固定储位；1：绑定固定储位；
     */
    @TableField("FIXEDCELL_FLAG")
    private String fixedcellFlag;

    /**
     * 数据权限类型
     */
    @TableField("AUTHORITY_TYPE")
    private String authorityType;

    /**
     * 上架策略
     */
    @TableField("I_STRATEGY")
    private String iStrategy;

    /**
     * 下架策略
     */
    @TableField("O_STRATEGY")
    private String oStrategy;

    /**
     * 补货策略
     */
    @TableField("M_STRATEGY")
    private String mStrategy;

    /**
     * 返配策略
     */
    @TableField("RI_STRATEGY")
    private String riStrategy;

    /**
     * 退货策略
     */
    @TableField("RO_STRATEGY")
    private String roStrategy;

    /**
     * 盘点策略
     */
    @TableField("FC_STRATEGY")
    private String fcStrategy;

    /**
     * 预留策略1
     */
    @TableField("RSV_STRATEGY1")
    private String rsvStrategy1;

    /**
     * 预留策略2
     */
    @TableField("RSV_STRATEGY2")
    private String rsvStrategy2;

    /**
     * 预留策略3
     */
    @TableField("RSV_STRATEGY3")
    private String rsvStrategy3;

    /**
     * 预留策略4
     */
    @TableField("RSV_STRATEGY4")
    private String rsvStrategy4;

    /**
     * 预留策略5
     */
    @TableField("RSV_STRATEGY5")
    private String rsvStrategy5;

    /**
     * 预留策略6
     */
    @TableField("RSV_STRATEGY6")
    private String rsvStrategy6;

    /**
     * 建立人员
     */
    @TableField("RGST_NAME")
    private String rgstName;

    /**
     * 建立日期
     */
    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;

    /**
     * 更新人员
     */
    @TableField("UPDT_NAME")
    private String updtName;

    /**
     * 更新日期
     */
    @TableField("UPDT_DATE")
    private LocalDateTime updtDate;

    /**
     * 自动采集箱码标识，0：不自动采集；1：自动采集
     */
    @TableField("SCAN_FLAG")
    private String scanFlag;

    /**
     * 企业编码
     */
    @TableId("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 储位管理类型，0：默认，做精细化管理；1：大储位管理
     */
    @TableField("CELL_MANAGER_TYPE")
    private String cellManagerType;

    /**
     * 默认储位，当CELL_MANAGER_TYPE设置为1时此字段才有效
     */
    @TableField("TYPE_VALUE")
    private String typeValue;

    @TableField("ROW_ID")
    private Long rowId;

    /**
     * 该货主导入进货单据时是否能自动产生商品对应的包装，0:不能；1:能。该预留属性有做导入的逻辑判断，不能做其它用途
     */
    @TableField("RSV_VAR1")
    private String rsvVar1;

    /**
     * 报关号
     */
    @TableField("RSV_VAR2")
    private String rsvVar2;

    /**
     * 账册
     */
    @TableField("RSV_VAR3")
    private String rsvVar3;

    @TableField("RSV_VAR4")
    private String rsvVar4;

    @TableField("RSV_VAR5")
    private String rsvVar5;

    @TableField("RSV_VAR6")
    private String rsvVar6;

    @TableField("RSV_VAR7")
    private String rsvVar7;

    @TableField("RSV_VAR8")
    private String rsvVar8;

    @TableField("RSV_NUM1")
    private Double rsvNum1;

    @TableField("RSV_NUM2")
    private Double rsvNum2;

    @TableField("RSV_NUM3")
    private Double rsvNum3;

    @TableField("RSV_DATE1")
    private LocalDateTime rsvDate1;

    @TableField("RSV_DATE2")
    private LocalDateTime rsvDate2;

    @TableField("RSV_DATE3")
    private LocalDateTime rsvDate3;

    /**
     * 仓库号
     */
    @TableField("WAREHOUSE_NO")
    private String warehouseNo;


}
