package com.hnblc.blcwms.persistent.datasource;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(-100) //这是为了保证AOP在事务注解之前生效,Order的值越小,优先级越高
@Slf4j
public class DataSourceSwitchAspect {

    @Pointcut("execution(* com.hnblc.blcwms.persistent.interfaces..*.*(..))")
    private void db1Aspect() {
    }

    @Pointcut("execution(* com.hnblc.blcwms.persistent.business..*.*(..))")
    private void db2Aspect() {
    }

    @Pointcut("execution(* com.hnblc.blcwms.persistent.ecssent..*.*(..))")
    private void db3Aspect(){

    }
    @Before( "db1Aspect()" )
    public void db1() {
        log.debug("switch to interfaces datasource（切换到接口库）...");
        DbContextHolder.setDbType(DBTypeEnum.apiDB);
    }

    @Before("db2Aspect()" )
    public void db2 () {
        log.debug("switch to business datasource（切换到业务库）...");
        DbContextHolder.setDbType(DBTypeEnum.businessDB);
    }
    @Before("db3Aspect()" )
    public void db3 () {
        log.debug("switch to customs ECSSENT datasource（切换到海关企业端库）...");
        DbContextHolder.setDbType(DBTypeEnum.ecssentDB);
    }
}


