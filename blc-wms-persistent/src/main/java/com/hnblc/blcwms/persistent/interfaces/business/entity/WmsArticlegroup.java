package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_ARTICLEGROUP")
public class WmsArticlegroup implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 传单单据编号：传单通讯使用(建议使用自动增加的系列号)
     */
    @TableId("SHEETID")
    private String sheetid;

    /**
     * 类别编码
     */
    @TableField("GROUP_NO")
    private String groupNo;

    /**
     * 类别名称
     */
    @TableField("GROUP_NAME")
    private String groupName;

    /**
     * 类别级别：1:大类 2:中类 3:小类
     */
    @TableField("LEVELID")
    private Double levelid;

    /**
     * 上级类别
     */
    @TableField("HEADGROUP_NO")
    private String headgroupNo;

    /**
     * 企业编码：无需插入值，仅保留默认值即可
     */
    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    /**
     * 委托业主编码：无需插入值，仅保留默认值即可
     */
    @TableField("OWNER_NO")
    private String ownerNo;

    /**
     * 接收时间
     */
    @TableField("HANDLETIME")
    private LocalDateTime handletime;


}
