package com.hnblc.blcwms.persistent.business.basedefine.mapper;

import java.util.Map;

public interface WmsBaseMapper {
    /**
     * 获取单号
     * @param procedureMap
     */
    void getSheetNo(Map<String,Object> procedureMap);

    /**
     * 回写品项明细数据
     * @param procedureMap
     */
    void initUpdateSku(Map<String,Object> procedureMap);

    /**
     * 初始化出货单据状态跟踪
     * @param procedureMap
     */
    void initStatus(Map<String,Object> procedureMap);
}
