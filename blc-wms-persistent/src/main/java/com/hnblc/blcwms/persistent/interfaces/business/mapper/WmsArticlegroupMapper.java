package com.hnblc.blcwms.persistent.interfaces.business.mapper;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsArticlegroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-11-18
 */
public interface WmsArticlegroupMapper extends BaseMapper<WmsArticlegroup> {

}
