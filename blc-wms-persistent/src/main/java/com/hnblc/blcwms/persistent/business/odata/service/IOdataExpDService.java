package com.hnblc.blcwms.persistent.business.odata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpD;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpDeliveryInfoVO;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linsong
 * @since 2019-07-26
 */
public interface IOdataExpDService extends IService<OdataExpD> {


    /**
     *
     * @param expDetailVO 表头参数对象
     * @return
     */
    List<ExpDeliveryInfoVO> getExpDWithExpM(ExpressQueryVO expDetailVO);

}
