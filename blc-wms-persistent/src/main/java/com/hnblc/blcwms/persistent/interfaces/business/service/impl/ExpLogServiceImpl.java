package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnblc.blcwms.persistent.interfaces.business.entity.ExpLog;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.ExpLogMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IExpLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 接口操作出货单记录 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-06-25
 */
@Service
public class ExpLogServiceImpl extends ServiceImpl<ExpLogMapper, ExpLog> implements IExpLogService {

}
