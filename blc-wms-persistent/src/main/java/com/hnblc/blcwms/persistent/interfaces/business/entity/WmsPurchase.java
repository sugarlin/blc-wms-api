package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("WMS_PURCHASE")
public class WmsPurchase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 1、采购单号2、验收通知单单号（ERP中采购订单）
     */
    @TableId("SHEETID")
    private String sheetid;

    /**
     * 1、商品编码2、进货商品编码 （对应 OWNER_ARTICLE_NO，其它业务相同）
     */
    @TableField("GOODSID")
    private String goodsid;

    /**
     * 商品条码
     */
    @TableField("BARCODE")
    private String barcode;

    /**
     * 1、委托业主编码2、委托业主编码，默认传0值
     */
    @TableField("CUSTOMID")
    private String customid;

    /**
     * 仓库编码，默认值001；
     */
    @TableField("PALLETZONE")
    private String palletzone;

    /**
     * 进货供货商编码（调入单：来源仓库的组织编码）
     */
    @TableField("VENDERID")
    private String venderid;

    /**
     * 对应至原有采购单号码（暂时用不上）
     */
    @TableField("REFSHEETID")
    private String refsheetid;

    /**
     * 采购单审核日期
     */
    @TableField("SDATE")
    private LocalDateTime sdate;

    /**
     * 制单员姓名
     */
    @TableField("CHECKER")
    private String checker;

    /**
     * 类型  保管型=’0’、直通型=’1’默认为保管型
     */
    @TableField("TYPE")
    private String type;

    /**
     * 送货日期
     */
    @TableField("PURDATE")
    private LocalDateTime purdate;

    /**
     * 单据有效期至：本单据之有效日期，超过此日则失效
     */
    @TableField("VALIDDATE")
    private LocalDateTime validdate;

    /**
     * 单内序号
     */
    @TableField("SERIALID")
    private Double serialid;

    /**
     * 包装：进货商品的包装系数（定货规格）
     */
    @TableField("PKCOUNT")
    private Double pkcount;

    /**
     * 1、采购数量2、进货总量
     */
    @TableField("QTY")
    private Double qty;

    /**
     * 说明
     */
    @TableField("NOTES")
    private String notes;

    /**
     * ERP的机构代码 （ERP 系统的仓库代码）
     */
    @TableField("SHOP_NO")
    private String shopNo;

    @TableField("ORDERUNIT")
    private String orderunit;

    @TableField("ORDERQUANTITY")
    private Double orderquantity;

    @TableField("UNITRATE")
    private Double unitrate;


}
