package com.hnblc.blcwms.persistent.interfaces.business.service;

import com.hnblc.blcwms.persistent.interfaces.business.entity.ConvertCustSet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linsong
 * @since 2019-11-26
 */
public interface IConvertCustSetService extends IService<ConvertCustSet> {

}
