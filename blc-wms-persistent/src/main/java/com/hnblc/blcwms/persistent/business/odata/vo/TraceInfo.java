package com.hnblc.blcwms.persistent.business.odata.vo;

import lombok.Data;

import java.util.Date;

@Data
public class TraceInfo {

    /**
     * 出货单号
     */
    private String expNo;
    private String orderNo;
    private String logisticNo;
    private String articleNo;
    private String ownerArticleNo;
    private String articleIdentifier;
    private String articleName;
    private String importBatchNo;
    private String qty;
    private String poNo;
    private Date expireDate;
    private Date checkEndDate;
}
