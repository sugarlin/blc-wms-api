package com.hnblc.blcwms.persistent.interfaces.business.mapper;

import com.hnblc.blcwms.persistent.interfaces.business.entity.ExpLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 接口操作出货单记录 Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-06-25
 */
public interface ExpLogMapper extends BaseMapper<ExpLog> {

}
