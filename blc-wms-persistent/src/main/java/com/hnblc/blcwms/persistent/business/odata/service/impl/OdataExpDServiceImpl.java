package com.hnblc.blcwms.persistent.business.odata.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpD;
import com.hnblc.blcwms.persistent.business.odata.mapper.OdataExpDMapper;
import com.hnblc.blcwms.persistent.business.odata.service.IOdataExpDService;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpDeliveryInfoVO;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-07-26
 */
@Service
public class OdataExpDServiceImpl extends ServiceImpl<OdataExpDMapper, OdataExpD> implements IOdataExpDService {

    @Override
    public List<ExpDeliveryInfoVO> getExpDWithExpM(ExpressQueryVO expressQueryVO) {
        return this.baseMapper.getDeliveryInfo(expressQueryVO);
    }

}
