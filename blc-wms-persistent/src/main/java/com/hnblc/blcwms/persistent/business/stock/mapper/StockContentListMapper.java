package com.hnblc.blcwms.persistent.business.stock.mapper;

import com.hnblc.blcwms.persistent.business.stock.entity.StockContentList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linsong
 * @since 2019-07-09
 */
public interface StockContentListMapper extends BaseMapper<StockContentList> {

}
