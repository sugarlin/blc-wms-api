package com.hnblc.blcwms.persistent.ecssent.entrence.service;

import com.hnblc.blcwms.persistent.ecssent.entrence.entity.PreBillHead;

import java.util.List;

public interface IPreBillService {

    List<PreBillHead> getGoodImportDeclinfo(List<String> billNo);
    PreBillHead getGoodImportDeclinfo(String billNo);
}
