package com.hnblc.blcwms.persistent.interfaces.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnblc.blcwms.persistent.interfaces.business.entity.ExpLog;

/**
 * <p>
 * 接口操作出货单记录 服务类
 * </p>
 *
 * @author linsong
 * @since 2019-06-25
 */
public interface IExpLogService extends IService<ExpLog> {

}
