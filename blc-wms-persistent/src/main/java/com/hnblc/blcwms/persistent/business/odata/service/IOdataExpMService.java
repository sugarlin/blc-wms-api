package com.hnblc.blcwms.persistent.business.odata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpM;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressInfoHead;
import com.hnblc.blcwms.persistent.business.odata.vo.ExpressQueryVO;
import com.hnblc.blcwms.persistent.business.odata.vo.TraceInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linsong
 * @since 2019-07-26
 */
public interface IOdataExpMService extends IService<OdataExpM> {

    List<TraceInfo> selectTraceInfo(String expNo);


    /**
     * 订单主要信息常用查询
     * @param queryVO
     * @return
     */
    List<ExpressInfoHead> getExpressFullInfo(ExpressQueryVO queryVO);
}
