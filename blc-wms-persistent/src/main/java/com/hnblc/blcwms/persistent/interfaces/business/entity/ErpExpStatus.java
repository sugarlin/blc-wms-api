package com.hnblc.blcwms.persistent.interfaces.business.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ERP_EXP_STATUS_NEW")
public class ErpExpStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("DELIVER_NO")
    private String deliverNo;

    @TableField("DELIVER_OBJ")
    private String deliverObj;

    @TableField("LOGISTIC_NO")
    private String logisticNo;

    @TableField("ENTERPRISE_NO")
    private String enterpriseNo;

    @TableField("WAREHOUSE_NO")
    private String warehouseNo;

    @TableField("OWNER_NO")
    private String ownerNo;

    @TableField("CUST_NO")
    private String custNo;

    @TableField("CLIENT_NAME")
    private String clientName;

    @TableField("CLIENT_ID")
    private String clientId;

    @TableField("LOADPROPOSE_NO")
    private String loadproposeNo;

    @TableField("CAR_PLAN_NO")
    private String carPlanNo;

    @TableField("SEND_NAME")
    private String sendName;

    @TableField("WORKER_NAME")
    private String workerName;

    @TableField("OPERATE_DATE")
    private LocalDateTime operateDate;


}
