package com.hnblc.blcwms.persistent.interfaces.business.dto;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: StockParam
 * @Description: TODO
 * @Author: linsong
 * @Date: 2019/8/27
 * @Version: V1.0
 */
@Data
public class StockParam {

    private String enterpriseNo;
    private String warehouseNo;
    private String ownerNo;
    List<StockQuery> articles;
}
