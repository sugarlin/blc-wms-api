package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsRet;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.WmsRetMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsRetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-12-17
 */
@Service
public class WmsRetServiceImpl extends ServiceImpl<WmsRetMapper, WmsRet> implements IWmsRetService {

}
