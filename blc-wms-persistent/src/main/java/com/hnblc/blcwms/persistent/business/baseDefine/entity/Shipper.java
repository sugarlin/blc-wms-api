package com.hnblc.blcwms.persistent.business.basedefine.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("BDEF_DEFSHIPPER")
public class Shipper implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 仓别编码
     */
    @TableField("WAREHOUSE_NO")
    private String warehouseNo;

    /**
     * 承运商编号
     */
    @TableField("SHIPPER_NO")
    private String shipperNo;

    /**
     * 承运商名称
     */
    @TableField("SHIPPER_NAME")
    private String shipperName;

    /**
     * 地址
     */
    @TableField("ADDRESS")
    private String address;

    /**
     * 电话
     */
    @TableField("TEL")
    private String tel;

    /**
     * 联系人
     */
    @TableField("CONTACT")
    private String contact;

    /**
     * 状态
     */
    @TableField("STATUS")
    private String status;

    /**
     * 里程单件
     */
    @TableField("DISPRICE")
    private Double disprice;

    /**
     * 重量单价
     */
    @TableField("GRAPRICE")
    private Double graprice;

    /**
     * 合同日期
     */
    @TableField("COMPACT_DATE")
    private LocalDateTime compactDate;

    /**
     * 到期日
     */
    @TableField("END_DATE")
    private LocalDateTime endDate;

    /**
     * 增值单位
     */
    @TableField("MULTI")
    private Double multi;

    /**
     * 备注
     */
    @TableField("MEMO")
    private String memo;

    /**
     * 材积单价
     */
    @TableField("VOLPRICE")
    private Double volprice;

    /**
     * 创建人
     */
    @TableField("RGST_NAME")
    private String rgstName;

    /**
     * 创建日期
     */
    @TableField("RGST_DATE")
    private LocalDateTime rgstDate;

    /**
     * 修改人
     */
    @TableField("UPDT_NAME")
    private String updtName;

    /**
     * 修改日期
     */
    @TableField("UPDT_DATE")
    private LocalDateTime updtDate;

    /**
     * 企业编码
     */
    @TableId("ENTERPRISE_NO")
    private String enterpriseNo;

    @TableField("REPORT_ID")
    private String reportId;

    /**
     * 面单类型 ：1：热敏标准；2：热敏个性；3：纸质标准；4：纸质个性
     */
    @TableField("PAPER_TYPE")
    private String paperType;

    /**
     * 是否单独拣货标识，0：不单独；1：单独
     */
    @TableField("SINGLE_LOCATE_FLAG")
    private String singleLocateFlag;

    /**
     * 承运商类型 ：1:快递公司；2：物流商
     */
    @TableField("SHIPPER_TYPE")
    private String shipperType;

    /**
     * 取快递单号方式：1、给号码段、2、按单取
     */
    @TableField("PAPER_COMIFIRE_FLAG")
    private String paperComifireFlag;

    /**
     * 和快递公司确认快递单号方式：1：按单取号同时确认；2：复核时确认
     */
    @TableField("GET_PAPER_TYPE")
    private String getPaperType;

    /**
     * 是否启用溯源二维码功能
     */
    @TableField("ENABLE_QRCODE")
    private String enableQrcode;


}
