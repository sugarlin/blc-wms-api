package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsRationnote;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.WmsRationnoteMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsRationnoteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-08-15
 */
@Service
public class WmsRationnoteServiceImpl extends ServiceImpl<WmsRationnoteMapper, WmsRationnote> implements IWmsRationnoteService {

}
