package com.hnblc.blcwms.persistent.interfaces.business.service;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsRationnote;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linsong
 * @since 2019-08-15
 */
public interface IWmsRationnoteService extends IService<WmsRationnote> {

}
