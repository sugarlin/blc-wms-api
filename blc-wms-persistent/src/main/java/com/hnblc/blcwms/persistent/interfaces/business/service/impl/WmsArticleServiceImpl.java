package com.hnblc.blcwms.persistent.interfaces.business.service.impl;

import com.hnblc.blcwms.persistent.interfaces.business.entity.WmsArticle;
import com.hnblc.blcwms.persistent.interfaces.business.mapper.WmsArticleMapper;
import com.hnblc.blcwms.persistent.interfaces.business.service.IWmsArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-19
 */
@Service
public class WmsArticleServiceImpl extends ServiceImpl<WmsArticleMapper, WmsArticle> implements IWmsArticleService {

}
