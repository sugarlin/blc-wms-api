package com.hnblc.blcwms.persistent.business.basedefine.service.impl;

import com.hnblc.blcwms.persistent.business.basedefine.entity.Platform;
import com.hnblc.blcwms.persistent.business.basedefine.mapper.PlatformMapper;
import com.hnblc.blcwms.persistent.business.basedefine.service.IPlatformService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-11-22
 */
@Service
public class PlatformServiceImpl extends ServiceImpl<PlatformMapper, Platform> implements IPlatformService {

}
