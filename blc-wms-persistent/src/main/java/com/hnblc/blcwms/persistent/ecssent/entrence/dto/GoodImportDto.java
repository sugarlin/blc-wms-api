package com.hnblc.blcwms.persistent.ecssent.entrence.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GoodImportDto {

    private String customsIdentifier;
    private String importDeclareNo;
}
