package com.hnblc.blcwms.persistent.interfaces.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnblc.blcwms.persistent.interfaces.message.entity.MessageLog;
import com.hnblc.blcwms.persistent.interfaces.message.mapper.MessageLogMapper;
import com.hnblc.blcwms.persistent.interfaces.message.service.IMessageLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 记录接口接收内容情况 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
@Service
public class MessageLogServiceImpl extends ServiceImpl<MessageLogMapper, MessageLog> implements IMessageLogService {

}
