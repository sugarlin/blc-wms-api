package com.hnblc.blcwms.persistent.interfaces.client.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnblc.blcwms.persistent.interfaces.client.dto.CurrentClient;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientAuth;
import com.hnblc.blcwms.persistent.interfaces.client.entity.ClientInfo;
import com.hnblc.blcwms.persistent.interfaces.client.mapper.ClientInfoMapper;
import com.hnblc.blcwms.persistent.interfaces.client.service.IClientInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 接口客户端信息表 服务实现类
 * </p>
 *
 * @author linsong
 * @since 2019-05-28
 */
@Service
public class ClientInfoServiceImpl extends ServiceImpl<ClientInfoMapper, ClientInfo> implements IClientInfoService {

    public CurrentClient loadCurrentClient(ClientAuth clientAuth){
        return this.baseMapper.loadCurrentClient(clientAuth);
    }
}
