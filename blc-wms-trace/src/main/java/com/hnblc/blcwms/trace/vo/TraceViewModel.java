package com.hnblc.blcwms.trace.vo;

import com.hnblc.blcwms.persistent.business.odata.vo.TraceInfo;
import com.hnblc.blcwms.persistent.ecssent.entrence.entity.PreBillHead;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TraceViewModel {


    private TraceInfo traceInfo;
    private PreBillHead preBillHead;
}
