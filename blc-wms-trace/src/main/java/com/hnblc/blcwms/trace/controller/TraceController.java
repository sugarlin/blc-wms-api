package com.hnblc.blcwms.trace.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hnblc.blcwms.common.constant.StringPool;
import com.hnblc.blcwms.common.exceptions.ErrorDescription;
import com.hnblc.blcwms.common.exceptions.GeneralException;
import com.hnblc.blcwms.common.interaction.RestResponse;
import com.hnblc.blcwms.common.utils.encrypt.Base64Utils;
import com.hnblc.blcwms.persistent.business.odata.entity.OdataExpM;
import com.hnblc.blcwms.persistent.business.odata.service.IOdataExpMService;
import com.hnblc.blcwms.persistent.business.odata.vo.TraceInfo;
import com.hnblc.blcwms.persistent.ecssent.entrence.service.IPreBillService;
import com.hnblc.blcwms.serviceapi.odata.IExpressService;
import com.hnblc.blcwms.serviceapi.odata.constant.ExpressConst;
import com.hnblc.blcwms.trace.vo.TraceViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TraceController {


    @Autowired
    IOdataExpMService odataExpMService;
    @Autowired
    IExpressService expressService;

    @Autowired
    IPreBillService preBillService;

    @RequestMapping("trace")
    public String getParceTrace(@RequestParam String traceCode, Model model){
        RestResponse restResponse = new RestResponse(true);

        String custNo;
        String orderNo;
        try {
            String[] param = Base64Utils.decodeString(traceCode).split(StringPool.COMMON_SEPARATOR_SPLIT_COMMA);
            custNo = param[0];
            orderNo = param[1];
        }catch (Exception e){
            restResponse.initError(new GeneralException(e));
            model.addAttribute("error", restResponse);
            return "index";
        }
        //判断订单操作状态

        OdataExpM odataExpM = odataExpMService.getOne(new QueryWrapper<OdataExpM>().eq("cust_no",custNo).eq("source_no",orderNo));

        if (odataExpM==null){
            restResponse.initError(new GeneralException(ErrorDescription.ERROR_WEB_EXPRESS_NOT_FOUND_0));
            model.addAttribute("error", restResponse);
            return "index";
        }
        if (!ExpressConst.ODATA_EXP_M__STATUS__SEND.equals(odataExpM.getStatus())){
            restResponse.initError(new GeneralException(ErrorDescription.ERROR_WEB_EXPRESS_WRONG_STATUS_0));
            model.addAttribute("error", restResponse);
            return "index";
        }
        //查询库存变动明细
        List<TraceViewModel> traceList = new ArrayList<>();
        List<TraceInfo> traceGoods= expressService.traceExpressGoods(odataExpM.getExpNo());

        List<String> billNoList = new ArrayList();
        traceGoods.forEach(item ->
                billNoList.add(item.getPoNo()));




//        List<PreBillHead> preBillHeadList = preBillService.getGoodImportDeclinfo(billNoList);
        traceGoods.forEach(item ->{
            //TODO 查询申报入区信息
            /*for (PreBillHead preBillHead : preBillHeadList){
                if (item.getPoNo().equalsIgnoreCase(preBillHead.get))
            }*/
            TraceViewModel traceViewModel = new TraceViewModel();
            traceViewModel.setTraceInfo(item);
            traceViewModel.setPreBillHead(preBillService.getGoodImportDeclinfo(item.getPoNo()));
            traceList.add(traceViewModel);
        });

        model.addAttribute("traceList",traceList);
       return "index";
    }

}
