package com.hnblc.blcwms.trace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ComponentScan("com.hnblc.blcwms")
@EnableCaching
@PropertySource("classpath:application-trace.properties")
public class BlcWmsTraceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlcWmsTraceApplication.class, args);
    }

}
